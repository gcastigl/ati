package ar.com.kiritsu.ati.test;

import java.util.List;

import org.junit.Test;

import ar.com.kiritsu.ati.image.entity.filter.FindNeighbors;
import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors;
import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors.ColorAndMaskPosition;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class NeighborsTest {

	@Test
	public void testGetNeighbors() {
		List<RGBColor> colors = Lists.newLinkedList();

		colors.add(new RGBColor(1, 0, 0));
		colors.add(new RGBColor(2, 0, 0));
		colors.add(new RGBColor(3, 0, 0));

		colors.add(new RGBColor(4, 0, 0));
		colors.add(new RGBColor(5, 0, 0));
		colors.add(new RGBColor(6, 0, 0));

		colors.add(new RGBColor(7, 0, 0));
		colors.add(new RGBColor(8, 0, 0));
		colors.add(new RGBColor(9, 0, 0));

		RGBImage image = new RGBImage(3, 3, colors);
		PixelNeighbors neighbors = new FindNeighbors(image, 3).at(1, 1);
		List<ColorAndMaskPosition> colorAndPoss = neighbors.valuesWithMiddle();
		int expected = 1;
		for (ColorAndMaskPosition colorAndPos : colorAndPoss) {
			Preconditions.checkArgument(colorAndPos.color().get().r() == expected++);
		}
	}

	@Test
	public void testGetNeighborsOnCorners() {
		List<RGBColor> colors = Lists.newLinkedList();

		colors.add(new RGBColor(1, 0, 0));
		colors.add(new RGBColor(2, 0, 0));
		colors.add(new RGBColor(3, 0, 0));

		colors.add(new RGBColor(4, 0, 0));
		colors.add(new RGBColor(5, 0, 0));
		colors.add(new RGBColor(6, 0, 0));

		colors.add(new RGBColor(7, 0, 0));
		colors.add(new RGBColor(8, 0, 0));
		colors.add(new RGBColor(9, 0, 0));

		RGBImage image = new RGBImage(3, 3, colors);
		PixelNeighbors neighbors = new FindNeighbors(image, 3).at(0, 0);
		List<ColorAndMaskPosition> colorAndPoss = neighbors.valuesWithMiddle();
		int index = 0;
		for (ColorAndMaskPosition colorAndPos : colorAndPoss) {
			if (index < 4 || index == 6) {
				Preconditions.checkArgument(!colorAndPos.color().isPresent());
			} else if (index == 4) {
				Preconditions.checkArgument(colorAndPos.color().get().r() == 1);
			} else {
				System.out.println(colorAndPos);
//				Preconditions.checkArgument(colorAndPos.color().get().r() == index);
			}
			index++;
		}
	}
}
