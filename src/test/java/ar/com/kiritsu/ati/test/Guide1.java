package ar.com.kiritsu.ati.test;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import ar.com.kiritsu.ati.image.entity.ImageBuilder;
import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.parser.RawImageParser;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;

public class Guide1 {

	private static final String OUT_DIR = "./src/main/resources/img/"; 

	public static void main(String[] args) throws IOException {
		Guide1 test = new Guide1();
//		test.circle();
//		test.sqare();
//		test.grayScale();
//		test.colorScale();
//		test.number6();
		test.pepe();
	}

	private final Map<String, Vector2I> _dimentions = Maps.newHashMap();
	
	public Guide1() {
		_dimentions.put("GIRL.RAW", new Vector2I(164, 389));
		_dimentions.put("BARCO.RAW", new Vector2I(207, 290));
		_dimentions.put("LENA.RAW", new Vector2I(256, 256));
		_dimentions.put("GIRL2.RAW", new Vector2I(256, 256));
		_dimentions.put("FRACTAL.RAW", new Vector2I(200, 200));
	}
	
	public void circle() throws IOException {
		int width = 200;
		int height = 200;
		final float r = 80;
		final int centerX = width / 2;
		final int centerY = height / 2;
		RGBImage image = new ImageBuilder(new Function<Vector2I, RGBColor>() {
			public RGBColor apply(Vector2I input) {
				if (distance(input.y(), input.x(), centerX, centerY) <= r) {
					return RGBColor.white();
				} else {
					return RGBColor.black();
				}
			}
		}).build(width, height);
		image.export(new File(OUT_DIR + "circular.png"));
	}
	
	public void sqare() throws IOException {
		int width = 200;
		int height = 200;
		final int sideLen = 100;
		
		final int centerX = width / 2;
		final int minX = centerX - sideLen / 2;
		final int maxX = centerX + sideLen / 2;

		final int centerY = height / 2;
		final int minY = centerY - sideLen / 2;
		final int maxY = centerY + sideLen / 2;

		RGBImage image = new ImageBuilder(new Function<Vector2I, RGBColor>() {
			public RGBColor apply(Vector2I input) {
				if (minY <= input.x() && input.x() <= maxY) {
					if (minX <= input.y() && input.y() <= maxX) {
						return RGBColor.white();
					}
				}
				return RGBColor.black();
			}
		}).build(width, height);
		image.export(new File(OUT_DIR + "square.png"));
	}
	
	private void grayScale() throws IOException {
		final int width = 512;
		final int height = 200;
		RGBImage image = new ImageBuilder(new Function<Vector2I, RGBColor>() {
			public RGBColor apply(Vector2I input) {
				float c = input.y() / (float) width;
				return new RGBColor(c, c, c);
			}
		}).build(width, height);
		image.export(new File(OUT_DIR + "grayscale.png"));
	}

	private void colorScale() throws IOException {
		final int width = 1024;
		final int height = 200;
		RGBImage image = new ImageBuilder(new Function<Vector2I, RGBColor>() {
			public RGBColor apply(Vector2I input) {
				RGBColor color;
				int column = input.y();
				float maxX1 = width / 3;
				float maxX2 = (2 * width) / 3;
				float maxX3 = width;
				if (column <= maxX1) {
					float r = 1 - (column / maxX1);
					float b = column / maxX1;
					color = new RGBColor(r, 0, b);
				} else if (column <= maxX2) {
					float b = (maxX2 - column) / maxX1;
					float g = 1 - (maxX2 - column) / maxX1;
					color = new RGBColor(0, g, b);					
				} else {
					float g = (maxX3 - column) / maxX1;
					float r = 1 - (maxX3 - column) / maxX1;
					color = new RGBColor(r, g, 0);
				}
				return color;
			}
		}).build(width, height);
		image.export(new File(OUT_DIR + "colorscale.png"));
	}

	private float distance(int x1, int y1, int x2, int y2) {
		int dx = x1 - x2;
		int dy = y1 - y2;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}
	
	private void number6() throws IOException {
		Function<File, Optional<RGBImage>> rawConverter = new RawImageParser(_dimentions);
		String file = "BARCO.RAW";
		Optional<RGBImage> shipImage = rawConverter.apply(new File("./src/main/resources/img/" + file));
		shipImage.get().export(new File(OUT_DIR + file + ".png"));
	}
	
	private void pepe() throws IOException {
		int width = 300;
		int height = 300;
		RGBImage image = new ImageBuilder(new Function<Vector2I, RGBColor>() {
			public RGBColor apply(Vector2I input) {
				return (input.row() == input.column()) ? RGBColor.white() : RGBColor.black();
			}
		}).build(width, height);
		image.export(new File(OUT_DIR + "square.png"));
	}
	
	
	
}
