package ar.com.kiritsu.ati.image.entity.parser;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Optional;

public class RGBImageFileParser implements ImageFileParser {

	@Override
	public Optional<RGBImage> apply(File input) {
		BufferedImage image;
		try {
			image = ImageIO.read(input);
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.absent();
		}
		List<RGBColor> pixels = new ArrayList<>(image.getWidth() * image.getHeight());
		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
		    	pixels.add(RGBColor.unpack(image.getRGB(x, y)));
			}
		}
		return Optional.of(new RGBImage(image.getWidth(), image.getHeight(), pixels));
	}

}
