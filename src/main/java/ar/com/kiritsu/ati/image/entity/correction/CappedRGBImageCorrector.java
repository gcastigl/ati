package ar.com.kiritsu.ati.image.entity.correction;

import ar.com.kiritsu.ati.image.entity.MathUtil;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

public class CappedRGBImageCorrector extends AbstractImageCorrector {

	public CappedRGBImageCorrector() {
		this(null);
	}

	public CappedRGBImageCorrector(String name) {
		super(name);
	}

	@Override
	public RGBColor convert(float r, float g, float b) {
		float red = capped(r);
		float green = capped(g);
		float blue = capped(b);
		return RGBColor.instance(red, green, blue);
	}

	private float capped(float value) {
		return MathUtil.capped(value, 0, 1);
	}

}
