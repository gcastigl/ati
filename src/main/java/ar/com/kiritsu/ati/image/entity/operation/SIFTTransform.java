package ar.com.kiritsu.ati.image.entity.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import mpi.cbg.fly.Feature;
import mpi.cbg.fly.SIFT;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.operation.api.ImagePairTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

@SuppressWarnings("serial")
public class SIFTTransform implements ImagePairTransform {

	private static final RGBColor[] COLORS = {
		new RGBColor(1, 0, 0),
		new RGBColor(0, 1, 0),
		new RGBColor(0, 0, 1),
		new RGBColor(1, 1, 0),
		new RGBColor(1, 0, 1),
		new RGBColor(0, 1, 1),
	};

	@SuppressWarnings("unused")
	private final float _threshold;

	public SIFTTransform(float threshold) {
		Preconditions.checkArgument(threshold >= 0);
		_threshold = threshold;
	}

	@Override
	public RGBImage apply(Pair<RGBImage, RGBImage> input) {
		RGBImage left = input.getLeft();
		RGBImage right = input.getRight();
		Vector<Feature> featuresl = SIFT.getFeatures(left.build().get());
		Vector<Feature> featuresr = SIFT.getFeatures(right.build().get());
		RGBImage result = buildResultIMage(left, right);
		int columnOffset = left.width();
		int colorIndex = 0;
		for (Pair<Feature, Feature> match : analyzeFeatures(featuresl, featuresr)) {
			traceLine(match, result, columnOffset, COLORS[colorIndex++]);
			colorIndex %= COLORS.length;
		}
		return result;
	}

	private RGBImage buildResultIMage(RGBImage left, RGBImage right) {
		List<RGBColor> colors = new ArrayList<>(left.size() + right.size());
		int h = Math.max(left.height(), right.height());
		int w = left.width() + right.width();
		for (int row = 0; row < h; row++) {
			for (int column = 0; column < left.width(); column++) {
				if (row >= left.height()) {
					colors.add(RGBColor.white());
				} else {
					colors.add(left.colorAt(row, column));
				}
			}
			for (int column = 0; column < right.width(); column++) {
				if (row >= right.height()) {
					colors.add(RGBColor.white());
				} else {
					colors.add(right.colorAt(row, column));
				}
			}
		}
		return new RGBImage(w, h, colors);
	}

	protected List<Pair<Feature, Feature>> analyzeFeatures(Vector<Feature> f1s, Vector<Feature> f2s) {
		List<Pair<Feature, Feature>> coincidences = Lists.newLinkedList();
		for (Feature f1 : f1s) {
			for (Feature f2 : f2s) {
				if (f1.descriptorDistance(f2) < 0.1f) {
					coincidences.add(Pair.of(f1, f2));
				}
			}
		}
		return coincidences;
	}

	@Override
	public String getName() {
		return "SIFT";
	}

	private void traceLine(Pair<Feature, Feature> match, RGBImage result, int columnOffset, RGBColor color) {
		float[] leftMatch = match.getLeft().location;
		float x0 = leftMatch[1];
		float y0 = leftMatch[0];
		float[] rightMatch = match.getRight().location;
		float x1 = rightMatch[1];
		float y1 = columnOffset + rightMatch[0];
		float dx = x1 - x0;
		float dy = y1 - y0;
		float mod = (float) Math.sqrt(dx * dx + dy * dy);
		dx /= mod; dy /= mod; 
		while (x0 <= x1 && y0 <= y1) {
			x0 += dx;
			y0 += dy;
			result.colorAt((int) x0, (int) y0).set(color);
		}
	}
}
