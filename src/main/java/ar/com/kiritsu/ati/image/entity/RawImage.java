package ar.com.kiritsu.ati.image.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.google.common.base.Preconditions;

public class RawImage {

	private File _file;
	private int _width, _height;

	public RawImage(File file, int width, int height) {
		_file = Preconditions.checkNotNull(file);
		_width = width;
		_height = height;
	}

	public File file() {
		return _file;
	}

	public int height() {
		return _height;
	}

	public int width() {
		return _width;
	}

	public List<Integer> getBytes() throws FileNotFoundException, IOException {
		List<Integer> unsigned = new ArrayList<>((int) _file.length());
		for (byte signedByte : IOUtils.toByteArray(new FileInputStream(_file))) {
			unsigned.add(signedByte & 0xFF); // XXX: color is signed!!
		}
		return unsigned;
	}

}
