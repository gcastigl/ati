package ar.com.kiritsu.ati.image.entity.noise;

public class MultiplicativeNoise extends AbstractImageNoise {

	public MultiplicativeNoise() {
		super("Ruido Multiplicativo");
	}

	@Override
	public float randomize(float n) {
		return n * generator().get();
	}

}
