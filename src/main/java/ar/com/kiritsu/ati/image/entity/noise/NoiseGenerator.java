package ar.com.kiritsu.ati.image.entity.noise;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.random.RandomGenerator;

public interface NoiseGenerator extends ImageTransform {

	NoiseGenerator setRandomGenerator(RandomGenerator generator);

}
