package ar.com.kiritsu.ati.image.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Vector2I implements Serializable {

	private int _x, _y;

	public Vector2I() {
		this(0, 0);
	}

	public Vector2I(int x, int y) {
		_x = x;
		_y = y;
	}

	public int x() {
		return _x;
	}

	public void setX(int x) {
		_x = x;
	}

	public int y() {
		return _y;
	}

	public void setY(int y) {
		_y = y;
	}

	public int row() {
		return y();
	}

	public void setRow(int row) {
		setY(row);
	}

	public int column() {
		return x();
	}

	public void setColumn(int column) {
		setX(column);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Vector2I)) {
			return false;
		}
		Vector2I other = (Vector2I) obj;
		return other.x() == x() && other.y() == y();
	}

	@Override
	public int hashCode() {
		return Integer.valueOf(x()).hashCode() + 31 * Integer.valueOf(y()).hashCode();
	}
	
	@Override
	public String toString() {
		return "(" + row() + ", " + column() + ")";
	}
}
