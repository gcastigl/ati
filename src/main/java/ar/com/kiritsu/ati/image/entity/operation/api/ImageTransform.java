package ar.com.kiritsu.ati.image.entity.operation.api;

import ar.com.kiritsu.ati.image.entity.Named;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;

public interface ImageTransform extends Function<RGBImage, RGBImage>, Named {

}
