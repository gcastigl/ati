package ar.com.kiritsu.ati.image.util;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.FileImageOutputStream;

public class GifSequenceWriter {

	/**
	 * public GifSequenceWriter( BufferedOutputStream outputStream, int
	 * imageType, int timeBetweenFramesMS, boolean loopContinuously) {
	 */

	public static void main(String[] args) throws Exception {
		if (args.length > 1) {
			BufferedImage firstImage = ImageIO.read(new File(args[0]));
			GifSequenceWriter writer = new GifSequenceWriter(new File(args[args.length - 1]), firstImage.getType(), 1, false);
			for (int i = 0; i < args.length - 1; i++) {
				writer.writeToSequence(ImageIO.read(new File(args[i])));
			}
			writer.close();
		} else {
			System.out.println("Usage: java GifSequenceWriter [list of gif files] [output file]");
		}
	}

	private ImageWriter gifWriter;
	private ImageWriteParam imageWriteParam;
	private IIOMetadata imageMetaData;
	private FileImageOutputStream _outputStream;

	public GifSequenceWriter(File output, int imageType, int timeBetweenFramesMS, boolean loopContinuously) {
		try {
			_outputStream = new FileImageOutputStream(output);
			build(imageType, timeBetweenFramesMS, loopContinuously);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeToSequence(RenderedImage img) {
		try {
			gifWriter.writeToSequence(new IIOImage(img, null, imageMetaData), imageWriteParam);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void build(int imageType, int timeBetweenFramesMS, boolean loopContinuously) throws IOException {
		// my method to create a writer
		gifWriter = getWriter();
		imageWriteParam = gifWriter.getDefaultWriteParam();
		ImageTypeSpecifier imageTypeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(imageType);

		imageMetaData = gifWriter.getDefaultImageMetadata(imageTypeSpecifier, imageWriteParam);

		String metaFormatName = imageMetaData.getNativeMetadataFormatName();

		IIOMetadataNode root = (IIOMetadataNode) imageMetaData.getAsTree(metaFormatName);

		IIOMetadataNode graphicsControlExtensionNode = getNode(root, "GraphicControlExtension");

		graphicsControlExtensionNode.setAttribute("disposalMethod", "none");
		graphicsControlExtensionNode.setAttribute("userInputFlag", "FALSE");
		graphicsControlExtensionNode.setAttribute("transparentColorFlag", "FALSE");
		// TODO: possible error here!!
		graphicsControlExtensionNode.setAttribute("delayTime", Integer.toString(timeBetweenFramesMS / 10));
		graphicsControlExtensionNode.setAttribute("transparentColorIndex", "0");

		IIOMetadataNode commentsNode = getNode(root, "CommentExtensions");
		commentsNode.setAttribute("CommentExtension", "Created by MAH");

		IIOMetadataNode appEntensionsNode = getNode(root, "ApplicationExtensions");

		IIOMetadataNode child = new IIOMetadataNode("ApplicationExtension");

		child.setAttribute("applicationID", "NETSCAPE");
		child.setAttribute("authenticationCode", "2.0");

		int loop = loopContinuously ? 0 : 1;

		child.setUserObject(new byte[] { 0x1, (byte) (loop & 0xFF), (byte) ((loop >> 8) & 0xFF) });
		appEntensionsNode.appendChild(child);

		imageMetaData.setFromTree(metaFormatName, root);

		gifWriter.setOutput(_outputStream);

		gifWriter.prepareWriteSequence(null);
	}

	/**
	 * Close this GifSequenceWriter object. This does not close the underlying
	 * stream, just finishes off the GIF.
	 */
	public void close() {
		try {
			gifWriter.endWriteSequence();
			_outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns an existing child node, or creates and returns a new child node
	 * (if the requested node does not exist).
	 * 
	 * @param rootNode
	 *            the <tt>IIOMetadataNode</tt> to search for the child node.
	 * @param nodeName
	 *            the name of the child node.
	 * 
	 * @return the child node, if found or a new node created with the given
	 *         name.
	 */
	private static IIOMetadataNode getNode(IIOMetadataNode rootNode, String nodeName) {
		int nNodes = rootNode.getLength();
		for (int i = 0; i < nNodes; i++) {
			if (rootNode.item(i).getNodeName().compareToIgnoreCase(nodeName) == 0) {
				return ((IIOMetadataNode) rootNode.item(i));
			}
		}
		IIOMetadataNode node = new IIOMetadataNode(nodeName);
		rootNode.appendChild(node);
		return (node);
	}

	private static ImageWriter getWriter() {
		Iterator<ImageWriter> iter = ImageIO.getImageWritersBySuffix("gif");
		if (iter.hasNext()) {
			return iter.next();
		}
		throw new IllegalStateException("No GIF Image Writers Exist");
	}
}