package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

public class ToBlackAndWhite extends PunctualImageTransform {

	public ToBlackAndWhite() {
		setTransformation(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				float c = (input.color().r() + input.color().g() + input.color().b()) / 3;
				return RGBColor.instance(c, c, c);
			}
		});
	}
}
