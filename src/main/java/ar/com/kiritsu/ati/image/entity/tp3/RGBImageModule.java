package ar.com.kiritsu.ati.image.entity.tp3;

import static org.apache.commons.lang3.tuple.Pair.of;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.operation.api.ImagePairTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColors;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;

@SuppressWarnings("serial")
public class RGBImageModule implements ImagePairTransform {

	@Override
	public RGBImage apply(Pair<RGBImage, RGBImage> input) {
		Preconditions.checkArgument(input.getLeft().size() == input.getRight().size());
		int size = input.getLeft().size();
		List<RGBColor> colors = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			RGBColor c1 = input.getLeft().colors().get(i);
			RGBColor c2 = input.getRight().colors().get(i);
			colors.add(RGBColors.module().apply(of(c1, c2)));
		}
		return new RGBImage(input.getLeft().dimentions(), colors);
	}

	@Override
	public String getName() {
		return "Modulo de de dos imagenes";
	}

}
