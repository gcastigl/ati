package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

public class NegateImage extends PunctualImageTransform {

	public NegateImage() {
		super(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				return RGBColor.instance(1 - input.color().r(), 1 - input.color().g(), 1 - input.color().b());
			}
		});
	}

}
