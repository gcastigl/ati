package ar.com.kiritsu.ati.image.entity.filter;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;

public abstract class ImageConvolution implements ImageTransform {

	private final int _windowSideLen;

	public ImageConvolution(int windowSideLen) {
		Preconditions.checkArgument(windowSideLen > 0);
		_windowSideLen = windowSideLen;
	}

	@Override
	public RGBImage apply(RGBImage input) {
		FindNeighbors findNeighbors = new FindNeighbors(input, _windowSideLen);
		List<RGBColor> colors = new ArrayList<>(input.size());
		for (int row = 0; row < input.height(); row++) {
			for (int column = 0; column < input.width(); column++) {
				colors.add(getColor(findNeighbors.at(row, column)));
			}
		}
		return new RGBImage(input.width(), input.height(), colors);
	}

	public int windowSideLen() {
		return _windowSideLen;
	}

	@Override
	public String name() {
		return getClass().getName();
	}

	public abstract RGBColor getColor(PixelNeighbors neighbors);

}
