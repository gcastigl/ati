package ar.com.kiritsu.ati.image.random;

public class RayleighRandomDistribution implements RandomGenerator {

	private float _eta;

	public RayleighRandomDistribution(float eta) {
		_eta = eta;
	}

	@Override
	public float get() {
		float x = (float) Math.random();
		float random = (float) Math.sqrt(-2 * Math.log(1 - x));
		return _eta * random;
	}

}
