package ar.com.kiritsu.ati.image.entity.operation;

import java.io.Serializable;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Function;

public interface RGBColorTransform extends Function<RGBColor, RGBColor>, Serializable {

}
