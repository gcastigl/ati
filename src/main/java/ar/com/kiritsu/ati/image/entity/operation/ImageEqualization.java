package ar.com.kiritsu.ati.image.entity.operation;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImageHistogram;

public class ImageEqualization implements ImageTransform {

	@Override
	public String name() {
		return "Equalizacion";
	}

	@Override
	public RGBImage apply(RGBImage input) {
		RGBImageHistogram histogram = new RGBImageHistogram(input);
		float[] redSks = new float[RGBColor.DEPTH];
		float[] greenSks = new float[RGBColor.DEPTH];
		float[] blueSks = new float[RGBColor.DEPTH];
		for (int k = 0; k < RGBColor.DEPTH; k++) {
			float[] prevSk = (k == 0) ? new float[3] : new float[] { redSks[k - 1], greenSks[k - 1], blueSks[k - 1] };
			float[] sk = calcSk(histogram, prevSk, k);
			redSks[k] = sk[0];
			greenSks[k] = sk[1];
			blueSks[k] = sk[2];
		}
		float minRed = findMinSk(redSks);
		float minGreen = findMinSk(greenSks);
		float minBlue = findMinSk(blueSks);
		for (int k = 0; k < RGBColor.DEPTH; k++) {
			redSks[k] = format(redSks[k], minRed);
			greenSks[k] = format(greenSks[k], minGreen);
			blueSks[k] = format(blueSks[k], minBlue);
		}
		List<RGBColor> equalized = new ArrayList<>(input.size());
		for (RGBColor color : input.colors()) {
			float red = redSks[color.redByte()];
			float green = redSks[color.greenByte()];
			float blue = redSks[color.blueByte()];
			equalized.add(RGBColor.instance(red, green, blue));
		}
		return new RGBImage(input.dimentions(), equalized);
	}

	private float[] calcSk(RGBImageHistogram histogram, float[] prevSk, int k) {
		float r = prevSk[0];
		float g = prevSk[1];
		float b = prevSk[2];
		float size = histogram.getTotalPixels();
		r += histogram.getRedHist()[k] / size;
		g += histogram.getGreenHist()[k] / size;
		b += histogram.getBlueHist()[k] / size;
		return new float[] { r, g, b };
	}

	private float findMinSk(float[] sks) {
		float min = Float.MAX_VALUE;
		for (float sk : sks) {
			min = Math.min(min, sk);
		}
		return min;
	}

	private float format(float c, float minc) {
		float aux = ((c - minc) / (1 - minc));
		return aux;
	}
}
