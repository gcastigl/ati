package ar.com.kiritsu.ati.image.entity.parser;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Optional;

public class PGMImageFileParser implements ImageFileParser {

	@Override
	public Optional<RGBImage> apply(File input) {
		try {
			List<Integer> bytes = new ArrayList<>((int) input.length());
			for (byte signedByte : IOUtils.toByteArray(new FileInputStream(input))) {
				bytes.add(signedByte & 0xFF); // XXX: color is signed!!
			}
			bytes.remove(0);
			bytes.remove(0);
			bytes.remove(0);
			int width = readInt(bytes, 32);
			int height = readInt(bytes, 10);
			@SuppressWarnings("unused")
			int depth = readInt(bytes, 10);
			List<RGBColor> colors = new ArrayList<>(width * height);
			while (!bytes.isEmpty()) {
				colors.add(RGBColor.fromGrayScale(bytes.remove(0)));
			}
			return Optional.of(new RGBImage(width, height, colors));
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.absent();
		}
		
	}

	private int readInt(List<Integer> bytes, int mark) {
		int ascii0 = (int) '0';
		String number = "";
		int b;
		do {
			b = bytes.remove(0);
			if (b != mark) {
				number += ((int) b) - ascii0;
			}
		} while(b != mark);
		return Integer.valueOf(number);
	}
}
