package ar.com.kiritsu.ati.image.service.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.parser.ImageFileParser;
import ar.com.kiritsu.ati.image.entity.parser.PGMImageFileParser;
import ar.com.kiritsu.ati.image.entity.parser.RGBImageFileParser;
import ar.com.kiritsu.ati.image.entity.parser.RawImageParser;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.service.api.ImageService;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ImageServiceImpl implements ImageService {

	private static final ImageServiceImpl instance = new ImageServiceImpl();

	public static ImageServiceImpl instance() {
		return instance;
	}

	private static final String WORKING_DIRECTORY = "./src/main/resources/img";
	private static final Map<String, Vector2I> rawFilesDimentions = Maps.newHashMap();
	static {
		rawFilesDimentions.put("GIRL.RAW", new Vector2I(164, 389));
		rawFilesDimentions.put("BARCO.RAW", new Vector2I(207, 290));
		rawFilesDimentions.put("LENA.RAW", new Vector2I(256, 256));
		rawFilesDimentions.put("GIRL2.RAW", new Vector2I(256, 256));
		rawFilesDimentions.put("FRACTAL.RAW", new Vector2I(200, 200));
	}

	private ImageFileParser rawParser = new RawImageParser(rawFilesDimentions);
	private ImageFileParser rgbParser = new RGBImageFileParser();
	private ImageFileParser pgmParser = new PGMImageFileParser();

//	private transient LoadingCache<File, RGBImage> _loadedImages;

	private ImageServiceImpl() {
//		_loadedImages = CacheBuilder.newBuilder()
//			.build(new CacheLoader<File, RGBImage>() {
//				@Override
//				public RGBImage load(File key) {
//					System.out.println("Cargando imagen: " + key.getName());
//					return parse(key);
//				}
//			});
	}

	private RGBImage parse(File imageFile) {
		Optional<RGBImage> image = Optional.absent();
		String name = imageFile.getName().toLowerCase();
		if (name.endsWith("raw")) {
			image = rawParser.apply(imageFile);
		} else if (name.endsWith("png") || name.endsWith("jpg") || name.endsWith("jpeg")) {
			image = rgbParser.apply(imageFile);
		} else if (name.endsWith("pgm")) {
			image = pgmParser.apply(imageFile);
		}
		if (image.isPresent()) {
			return image.get();
		}
		throw new IllegalStateException("Image could not be built: " + imageFile);
	}

	@Override
	public List<File> findAllFiles() {
		List<File> _files = Lists.newLinkedList();
		File imagesDir = new File(WORKING_DIRECTORY);
		for (File imageFile : imagesDir.listFiles()) {
			String name = imageFile.getName().toLowerCase();
			if (name.endsWith("raw") || name.endsWith("png") || name.endsWith("jpg") || name.endsWith("pgm")) {
				_files.add(imageFile);
			} else {
				System.out.println("Ignored: " + imageFile.getName());
			}
		}
		return _files;
	}

	@Override
	public RGBImage build(File imageFile) {
//		return _loadedImages.getUnchecked(imageFile);
		return parse(imageFile);
	}

}
