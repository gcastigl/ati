package ar.com.kiritsu.ati.image.entity.rgb;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import ar.com.kiritsu.ati.image.entity.Vector2I;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

@SuppressWarnings("serial")
public class RGBImage implements Serializable {

	public static Optional<RGBImage> absent() {
		return Optional.absent();
	}

	private Vector2I _dimentions;
	private List<RGBColor> _colors;

	private transient BufferedImage _bufferedImage;

	public RGBImage() {
	}

	public RGBImage(int width, int height) {
		_dimentions = new Vector2I(width, height);
		_colors = Lists.newArrayList(new RGBColor[width * height]);
		validateDimention();
	}

	public RGBImage(int width, int height, List<RGBColor> colors) {
		this(new Vector2I(width, height), colors);
	}

	public RGBImage(Vector2I dimentions, List<RGBColor> colors) {
		Preconditions.checkArgument(dimentions.x() * dimentions.y() == colors.size());
		_dimentions = dimentions;
		_colors = new ArrayList<>(colors);
		validateDimention();
	}

	private void validateDimention() {
		Preconditions.checkArgument(0 <= width());
		Preconditions.checkArgument(0 <= height());
	}

	public boolean needsCorrection() {
		return Iterables.any(colors(), RGBColors.needsCorrection());
	}

	public Vector2I dimentions() {
		return _dimentions;
	}

	public int width() {
		return _dimentions.x();
	}

	public int height() {
		return _dimentions.y();
	}

	public int size() {
		return colors().size();
	}

	public List<RGBColor> colors() {
		return Collections.unmodifiableList(_colors);
	}

//	public Collection<RGBPixel> pixels() {
//		return Collections2.transform(colors(), new Function<RGBColor, RGBPixel>() {
//			private int _offset;
//			@Override
//			public RGBPixel apply(RGBColor input) {
//				int offset = _offset;
//				_offset++;
//				return new RGBPixel(input, offsetToRow(offset), offsetToColumn(offset));
//			}
//		});
//	}

	public String format() {
		return "png";
	}

	public void export(File file) throws IOException {
		ImageIO.write(build().get(), format(), file);
	}

	public Optional<BufferedImage> build() {
		try {
			_bufferedImage = new BufferedImage(width(), height(), BufferedImage.TYPE_INT_ARGB);
			int index = 0;
			Collection<Integer> argbs = Collections2.transform(colors(), RGBColors.packed());
			int[] data = ((DataBufferInt) _bufferedImage.getRaster().getDataBuffer()).getData();
			for (Integer args : argbs) {
				data[index++] = args;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.absent();
		}
		return Optional.of(_bufferedImage);
	}

	public void setSafe(int row, int column, RGBColor pixel) {
		if (0 <= row && row < height() && 0 <= column && column < width()) {
			set(row, column, pixel);
		}
	}

	public void set(int row, int column, RGBColor pixel) {
		colorAt(row, column).set(pixel.r(), pixel.g(), pixel.b());
	}

	public void set(int row, int column, float r, float g, float b) {
		colorAt(row, column).set(r, g, b);
	}

	public int offsetToRow(int offset) {
		return offset / width();
	}

	public int offsetToColumn(int offset) {
		return offset % width();
	}

	public RGBColor colorAt(Vector2I v) {
		return colorAt(v.row(), v.column());
	}

	public RGBColor colorAt(int row, int column) {
		int offset = offset(row, column);
		RGBColor pixel = colors().get(offset);
		if (pixel == null) {
			colors().set(offset, pixel = new RGBColor());
		}
		return pixel;
	}

	private int offset(int row, int column) {
		return row * width() + column;
	}

	public void subImage(int row, int column, int width, int height) {
		int start = offset(row, column);
		int end = offset(row + height, column + width);
		_colors = _colors.subList(start, end);
	}

	public RGBImage copy() {
		List<RGBColor> clonedColors = new ArrayList<RGBColor>(colors().size());
		for (RGBColor color : colors()) {
			clonedColors.add(new RGBColor(color));
		}
		return new RGBImage(dimentions(), clonedColors);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(width() + " * " + height());
		sb.append(" | " + colors().subList(0, 20));
		return sb.toString();
	}
}
