package ar.com.kiritsu.ati.image.entity.noise;

public class SaltAndPepperNoise extends AbstractImageNoise {

	private float _p, _p0, _p1;

	public SaltAndPepperNoise(float p, float p0, float p1) {
		super("Sal y pimienta");
		_p = p;
		this._p0 = p0;
		this._p1 = p1;
	}

	@Override
	public float randomize(float n) {
		if (_p <= generator().get()) {
			return n;
		}
		float x = generator().get();
		if (x <= _p0) {
			return 0;
		} else if (_p1 <= x) {
			return 1;
		}
		return n;
	}

}
