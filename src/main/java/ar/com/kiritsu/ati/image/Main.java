package ar.com.kiritsu.ati.image;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.correction.DynamicRangeCompression;
import ar.com.kiritsu.ati.image.entity.correction.LinearImageCorrector;
import ar.com.kiritsu.ati.image.entity.operation.Canny;
import ar.com.kiritsu.ati.image.entity.operation.ContrastImprovements;
import ar.com.kiritsu.ati.image.entity.operation.ImageEqualization;
import ar.com.kiritsu.ati.image.entity.operation.ImageThresholding;
import ar.com.kiritsu.ati.image.entity.operation.IsotropicDifussion;
import ar.com.kiritsu.ati.image.entity.operation.MultiplyImageByScalar;
import ar.com.kiritsu.ati.image.entity.operation.NegateImage;
import ar.com.kiritsu.ati.image.entity.operation.OtsuImageThresholding;
import ar.com.kiritsu.ati.image.entity.operation.RGBImageOperations;
import ar.com.kiritsu.ati.image.entity.operation.Susan;
import ar.com.kiritsu.ati.image.entity.operation.ToBinaryImage;
import ar.com.kiritsu.ati.image.entity.operation.ToBlackAndWhite;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImageHistogram;
import ar.com.kiritsu.ati.image.service.impl.ImageServiceImpl;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

public class Main {

	public static void main(String[] args) throws IOException {
		Options options = options();
		CommandLine cmd;
		try {
			cmd = new BasicParser().parse(options, args);
		} catch (ParseException e) {
			System.err.println("Invalid argument. Reason: " + e.getMessage());
			new HelpFormatter().printHelp("ant", options);
			return;
		}
		if (cmd.hasOption("help")) {
			new HelpFormatter().printHelp("ant", options);
			return;
		}
		final File input = new File(cmd.getOptionValue("i"));
		if (!input.exists()) {
			System.out.println("File " + input + " does not exists");
			return;
		}
		RGBImage inputImage = ImageServiceImpl.instance().build(input);
		List<String> optsToIgnore = Arrays.asList("i", "ii", "o", "saveall");
		List<Option> operations = Arrays.asList(cmd.getOptions()).stream().filter(o -> {
			return !optsToIgnore.contains(o.getOpt());
		}).collect(Collectors.toList());
		if (operations.isEmpty()) {
			System.out.println("[WARN] no operations specified.");
		}
		boolean saveAll = cmd.hasOption("saveall");
		for (Option option : operations) {
			System.out.println("Applying: " + option.getOpt());
			inputImage = Optional.fromNullable(applyOperation(inputImage, cmd, option)).or(inputImage);
			if (saveAll && !option.getArgName().equals("stats")) {
				File out = output(cmd, input, "_" + option.getOpt());
				System.out.println("Exporting to: " + out.getAbsolutePath());
				inputImage.export(out);
			}
		}
		final File output = output(cmd, input, "_final");
		System.out.println("Exporting to: " + output.getAbsolutePath());
		inputImage.export(output);
	}

	private static File output(CommandLine cmd, File input, String postfix) {
		File output;
		if (cmd.hasOption("o")) {
			output = new File(cmd.getOptionValue("o"));
		} else {
			output = input;
		}
		if (!Strings.isNullOrEmpty(postfix)) {
			String finalOutputname;
			String outputname = output.getName();
			int dotIndex = outputname.lastIndexOf('.');
			if (dotIndex != -1) {
				finalOutputname = outputname.substring(0, dotIndex) + postfix + outputname.substring(dotIndex);
			} else {
				finalOutputname = outputname + postfix;
			}
			output = new File(finalOutputname);
		}
		return output;
	}

	private static Options options() {
		Options options = new Options();
		options.addOption("help", false, "(Opcional) Mostrar menu de ayuda");
		options.addOption(required(new Option("i", true, "Archivo de entrada")));
		options.addOption("ii", true, "Archivo secundario de entrada");
		options.addOption("o", true, "Archivo de salida");
		options.addOption("stats", false, "Estadisticas de la imagen");
		options.addOption("saveall", false, "Guardar todas las imagenes intermedias");
		// Operations
		options.addOption("neg", false, "Negativo de la imagen");
		options.addOption("mult", true, "Multiplicar por escalar\t-\tt");
		options.addOption("binary", true, "Thresshold binario\t-\tthresshold [0,1]");
		options.addOption("blaknwhite", false, "Pasar a tono de grises");
		options.addOption(setArgs(new Option("contrast", true, "Mejora de contraste\t-\tthresshods min and max [0, 1]"), 2));
		options.addOption("eq", false, "Equalizacion de la imagen");
		options.addOption(setArgs(new Option("threshold", true, "Umbralizacion Global\t-\tt [0, 1] y dt > 0"), 2));
		options.addOption("otsu", false, "Umbralizacion Otsu");
		options.addOption("difiso", false, "Difusion isotropica");
		// Edge detector
		options.addOption(setArgs(new Option("cannny", true, "Canny\t-\tsize sigma t1 t2"), 4));
		options.addOption(setArgs(new Option("sussan", true, "Sussan\t-\tt > 0 dettectCornersOnly bool"), 2));
		// Compressor
		options.addOption("cdynamicrange", false, "Compresion de rango dinamico");
		options.addOption("clinear", false, "Compresion linear");
		return options;
	}

	private static final Option required(Option option) {
		option.setRequired(true);
		return option;
	}

	private static final Option setArgs(Option option, int num) {
		option.setArgs(num);
		return option;
	}

	private static RGBImage applyOperation(RGBImage input, CommandLine cmd, Option option) {
		RGBImage output = null;
		switch (option.getOpt()) {
		case "neg":
			output = new NegateImage().apply(input);
			break;
		case "mult":
			output = new MultiplyImageByScalar(Float.valueOf(option.getValue(0))).apply(input);
			break;
		case "binary":
			output = new ToBinaryImage(Float.valueOf(option.getValue(0))).apply(input);
			break;
		case "blaknwhite":
			output = new ToBlackAndWhite().apply(input);
			break;
		case "contrast": {
			float r1 = Float.valueOf(option.getValue(0));
			float r2 = Float.valueOf(option.getValue(1));
			output = ContrastImprovements.linear(r1, r2).apply(input);
		}
			break;
		case "eq":
			output = new ImageEqualization().apply(input);
			break;
		case "threshold": {
			float t = Float.valueOf(option.getValue(0));
			float dt = Float.valueOf(option.getValue(1));
			output = new ImageThresholding(t, dt).apply(input);
		}
			break;
		case "otsu":
			output = new OtsuImageThresholding().apply(input);
			break;
		case "difiso":
			output = new IsotropicDifussion().apply(input);
			break;
		case "canny": {
			int size = Integer.valueOf(option.getValue(0));
			float sigma = Float.valueOf(option.getValue(1));
			float t1 = Float.valueOf(option.getValue(2));
			float t2 = Float.valueOf(option.getValue(3));
			output = new Canny(size, sigma, t1, t2).apply(input);
		}
			break;
		case "sussan": {
			float t = Float.valueOf(option.getValue(0));
			boolean detectCornersOnly = option.getValue(1).equalsIgnoreCase("true");
			output = new Susan(t, detectCornersOnly, Optional.<Float> absent()).apply(input);
		}
			break;
		case "cdynamicrange":
			output = new DynamicRangeCompression().apply(input);
			break;
		case "clinear":
			output = new LinearImageCorrector().apply(input);
			break;
		case "stats":
			output = input;
			System.out.println(String.format("(Min, max) = %s", minAndMax(input)));
			RGBImageHistogram hist = new RGBImageHistogram(input);
			int[] reds = hist.getRedHist();
			int[] greens = hist.getGreenHist();
			int[] blues = hist.getBlueHist();
			for (int i = 0; i < RGBColor.DEPTH; i++) {
				if (reds[i] != 0 || greens[i] != 0 || blues[i] != 0) {
					System.out.println(String.format("%d\t%d\t%d\t%d", i, reds[i], greens[i], blues[i]));
				}
			}
			break;
		default:
			System.out.println("Unknown option " + option.getOpt());
			break;
		}
		return output;
	}

	private static Pair<RGBColor, RGBColor> minAndMax(RGBImage image) {
		return Pair.of(RGBImageOperations.min().apply(image), RGBImageOperations.max().apply(image));
	}
}
