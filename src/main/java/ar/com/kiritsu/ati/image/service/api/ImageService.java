package ar.com.kiritsu.ati.image.service.api;

import java.io.File;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

public interface ImageService {

	RGBImage build(File image);

	List<File> findAllFiles();

}
