package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.filter.ImageConvolution;
import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class Susan implements ImageTransform {

	private static final RGBColor DEFAULT = RGBColor.black();
	private SUSANConvolution _susanConvolution;

	public Susan(float t, boolean detectCornersOnly, Optional<Float> cornerMin) {
		_susanConvolution = new SUSANConvolution(t, detectCornersOnly, cornerMin.or(0.1f));
	}
	
	@Override
	public RGBImage apply(RGBImage input) {
		RGBImage transformed = _susanConvolution.apply(input);
		if (_susanConvolution.isDetectCornersOnly()) {
			RGBColor cornerColor = new RGBColor(1, 0, 0);
			RGBImage markedEdges = input.copy();
			for (int row = 0; row < input.height(); row++) {
				for (int column = 0; column < input.width(); column++) {
					if (transformed.colorAt(row, column).r() == 1) {
						markedEdges.set(row, column, cornerColor);
					}
				}
			}
			transformed = markedEdges;
		}
		return transformed;
	}

	@Override
	public String name() {
		return "S.U.S.A.N.";
	}

	private static class SUSANConvolution extends ImageConvolution {

		private final int N = 7 + (7 + 7) + (5 + 5) + (3 + 3);
		private final float _t;
		private float _eps;
		private boolean _detectCornersOnly;
		private float _expectedS;

		public SUSANConvolution(float t, boolean detectCornersOnly, float eps) {
			super(7);
			Preconditions.checkArgument(t > 0);
			Preconditions.checkArgument(eps > 0);
			_t = t;
			_eps = eps;
			_expectedS = detectCornersOnly ? 0.75f : 0.5f;
			_detectCornersOnly = detectCornersOnly;
		}
		
		public boolean isDetectCornersOnly() {
			return _detectCornersOnly;
		}

		@Override
		public RGBColor getColor(PixelNeighbors neighbors) {
			float r0 = neighbors.get(0, 0).get().r();
			float c = 0;
			c += sumLayer(neighbors, r0, 0, 3);
			c += sumLayer(neighbors, r0, 1, 3);
			c += sumLayer(neighbors, r0, 2, 2);
			c += sumLayer(neighbors, r0, 3, 1);
			float s = 1 - c / N;
			s = (Math.abs(s - _expectedS) < _eps) ? 1 : 0;
			return RGBColor.instance(s, s, s);
		}

		private float sumLayer(PixelNeighbors neighbors, float r0, int row, int len) {
			float c = 0;
			for (int i = -len; i <= len; i++) {
				c += evaluate(neighbors.get(-row, i).or(DEFAULT), r0);
				if (-row != row) {
					c += evaluate(neighbors.get(row, i).or(DEFAULT), r0);
				}
			}
			return c;
		}

		private float evaluate(RGBColor r, float r0) {
			return Math.abs(r.r() - r0) < _t ? 1 : 0;
		}
	}
}
