package ar.com.kiritsu.ati.image.entity;

import java.awt.Color;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Function;

public class RGBToHSV implements Function<RGBColor, HSVColor> {

	private final float[] _hsbvals = new float[3];

	@Override
	public HSVColor apply(RGBColor input) {
		Color.RGBtoHSB(input.redByte(), input.greenByte(), input.blueByte(), _hsbvals);
		return new HSVColor(_hsbvals[0], _hsbvals[1], _hsbvals[2]);
	}

}
