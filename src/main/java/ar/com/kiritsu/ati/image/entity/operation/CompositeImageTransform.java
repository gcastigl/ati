package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Functions;

public class CompositeImageTransform implements ImageTransform {

	private Function<RGBImage, RGBImage> _transform = Functions.identity();

	public CompositeImageTransform composed(ImageTransform transform) {
		_transform = Functions.compose(transform, _transform);
		return this;
	}

	@Override
	public RGBImage apply(RGBImage input) {
		return _transform.apply(input);
	}

	@Override
	public String name() {
		return _transform == null ? "null" : _transform.toString();
	}

}
