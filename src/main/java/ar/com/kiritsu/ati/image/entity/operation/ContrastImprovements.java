package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;

import com.google.common.base.Function;
import com.google.common.base.Functions;

public class ContrastImprovements {

	public static final ImageTransform linear(float r1, float r2) {
		Function<Float, Float> f1 = new Function<Float, Float>() {
			@Override
			public Float apply(Float input) {
				return input / 2;
			}
		};
		Function<Float, Float> f2 = Functions.identity();
		Function<Float, Float> f3 = new Function<Float, Float>() {
			@Override
			public Float apply(Float input) {
				return Math.min(1, input * 2);
			}
		};
		return new ContrastImprovement(f1, f2, f3, r1, r2);
	}
	
}
