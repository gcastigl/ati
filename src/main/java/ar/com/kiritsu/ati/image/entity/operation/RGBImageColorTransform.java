package ar.com.kiritsu.ati.image.entity.operation;

import java.io.Serializable;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.operation.RGBImageColorTransform.ColorAndPostition;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Function;

@SuppressWarnings("serial")
public interface RGBImageColorTransform extends Function<ColorAndPostition, RGBColor> {

	public static class ColorAndPostition implements Serializable {
		private RGBColor _color;
		private Vector2I _position;

		public ColorAndPostition(RGBColor color, Vector2I position) {
			_color = color;
			_position = position;
		}

		public RGBColor color() {
			return _color;
		}

		public Vector2I position() {
			return _position;
		}
	}
}
