package ar.com.kiritsu.ati.image.entity;

import java.util.Arrays;
import java.util.List;

public class SobelMask {

	private final List<Float> _xWeights;
	private final List<Float> _yWeights;

	public SobelMask() {
		_xWeights = Arrays.asList(new Float[] { 
			-1f, -2f, -1f, 
			0f, 0f, 0f, 
			1f, 2f, 1f 
		});
		_yWeights = Arrays.asList(new Float[] { 
			-1f, 0f, 1f, 
			-2f, 0f, 2f, 
			-1f, 0f, 1f 
		});
	}

	public List<Float> xWeights() {
		return _xWeights;
	}

	public List<Float> yWeights() {
		return _yWeights;
	}

	public int maskSize() {
		return 3;
	}
}
