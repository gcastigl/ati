package ar.com.kiritsu.ati.image.util;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;

public class FloatFunctions {

	public static Function<Float, Float> one() {
		return new Function<Float, Float>() {
			@Override
			public Float apply(Float input) {
				return 1f;
			}
		};
	}

	public static Function<Float, Float> expNeg(final float k) {
		return new Function<Float, Float>() {
			@Override
			public Float apply(Float input) {
				float exp = -(input * input) / (k * k);
				return (float) Math.pow(Math.E, exp);
			}
		};
	}

	public static List<Float> rotate3x3Matrix(List<Float> matrix) {
		Preconditions.checkArgument(matrix.size() == 9);
		List<Float> rotated = new ArrayList<>(9);
		rotated.add(matrix.get(1));
		rotated.add(matrix.get(2));
		rotated.add(matrix.get(5));
		rotated.add(matrix.get(0));
		rotated.add(matrix.get(4));
		rotated.add(matrix.get(8));
		rotated.add(matrix.get(3));
		rotated.add(matrix.get(6));
		rotated.add(matrix.get(7));
		return rotated;
	}

}
