package ar.com.kiritsu.ati.image.util;

import java.io.Serializable;

import com.google.common.base.Supplier;

public interface SerializableSupplier<T> extends Supplier<T>, Serializable {

}
