package ar.com.kiritsu.ati.image.entity.operation;

import static com.google.common.base.Functions.compose;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.SobelMask;
import ar.com.kiritsu.ati.image.entity.filter.GaussianFilter;
import ar.com.kiritsu.ati.image.entity.filter.GradientFilter;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;

public class Harris implements ImageTransform {

	private float k = 0.04f;
	private float _thresshold;

	private ImageTransform _gaussian;
	private ImageTransform _gx;
	private ImageTransform _gy;
	private Function<RGBImage, RGBImage> _squareAndGauss;

	public Harris(int maskSize, float sigma, float thresshold) {
		_thresshold = thresshold;
		final SobelMask sobelMask = new SobelMask();
		Optional<List<Float>> noList = Optional.absent();
		_gx = new GradientFilter(sobelMask.maskSize(), sobelMask.xWeights(), noList);
		_gy = new GradientFilter(sobelMask.maskSize(), sobelMask.yWeights(), noList);
		_gaussian = new GaussianFilter(maskSize, sigma);
		_squareAndGauss = compose(_gaussian, new PunctualImageTransform(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				float r = input.color().r();
				float rsq = r * r;
				return new RGBColor(rsq, rsq, rsq);
			}
		}));

	}

	@Override
	public String name() {
		return "Harris";
	}

	@Override
	public RGBImage apply(RGBImage input) {
		// Calcular lx e ly usando las mácaras de prewit o sobel para toda la
		// imagen
		RGBImage lx = _gx.apply(input);
		RGBImage ly = _gy.apply(input);

		// Calcular lx2 elevando lx^2 elemento a elemento y aplicar un filtro
		// gaussiano para suavizar. Lo mismo ly^2.
		final RGBImage lx2 = _squareAndGauss.apply(lx);
		final RGBImage ly2 = _squareAndGauss.apply(ly);

		// Paso3: Calcular lxy multiplicando elemento a elemnto también suavisar
		// y luego aplicar el mismo filtro gaussiano.
		RGBImage glxy = _gaussian.apply(new Multiply(ly2).apply(lx2));

		// Paso 4: cim = (lx2.* ly2 -­ lxy.^2) ­- k * (lx2 + ly2).^2;
		List<Float> cims = new ArrayList<>(input.size());
		for (int i = 0; i < lx.size(); i++) {
			float lx2r = lx2.colors().get(i).r();
			float ly2r = ly2.colors().get(i).r();
			float glxyr = glxy.colors().get(i).r();
			float aux = (lx2r + ly2r);
			cims.add((lx2r * ly2r - (glxyr * glxyr)) - k * (aux * aux));
		}
		RGBImage result = input.copy();
		// Paso 5: Encontrar los máximos
		int index = 0;
		for (float value : cims) {
			if (_thresshold < value) {
//				System.out.print(value + ", ");
				result.colors().get(index).set(1, 0, 0);
			}
			index++;
		}
		return result;
	}

	private static final class Multiply extends PunctualImageTransform {

		public Multiply(final RGBImage y) {
			setTransformation(new RGBImageColorTransform() {
				@Override
				public RGBColor apply(ColorAndPostition input) {
					RGBColor xColor = input.color();
					RGBColor yColor = y.colorAt(input.position());
					float r = xColor.r() * yColor.r();
					return new RGBColor(r, r, r);
				}
			});
		}
	}
	
}
