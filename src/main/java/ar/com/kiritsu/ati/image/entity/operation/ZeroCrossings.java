package ar.com.kiritsu.ati.image.entity.operation;

import java.util.List;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class ZeroCrossings implements ImageTransform {

	private final float _step;

	public ZeroCrossings(float step) {
		_step = step;
	}

	@Override
	public RGBImage apply(RGBImage input) {
		List<List<RGBColor>> rows = Lists.partition(input.colors(), input.width());
		List<RGBColor> allBorders = Lists.newArrayList();
		for (List<RGBColor> row : rows) {
			List<RGBColor> bordersOnRow = Lists.newArrayList();
			boolean initialized = false;
			int startIndex = -1;
			boolean positive = false;
			for (int i = 0; i < row.size(); i++) {
				RGBColor color = row.get(i);
				float r = color.r();
				if (r == 0) {
					bordersOnRow.add(RGBColor.white());
					continue;
				}
				if (!initialized) {
					initialized = true;
					positive = r > 0;
					startIndex = i;
				} else {
					if ((positive && r < 0) || (!positive && r > 0)) {
						// cruce por cero
						float m = (r - row.get(startIndex).r()) / (i - startIndex);
						if (Math.abs(m) > _step) {
							// posible borde
							int borderIndex = startIndex + (i - startIndex) / 2;
							bordersOnRow.set(borderIndex, RGBColor.black());
//							bordersOnRow.set(Math.max(borderIndex - 1, 0), RGBColor.BLACK);
						}
						positive = !positive;
						startIndex = i;
					} else {
						startIndex = i;
					}
				}
				bordersOnRow.add(RGBColor.white());
			}
			Preconditions.checkArgument(bordersOnRow.size() == input.width());
			allBorders.addAll(bordersOnRow);
		}
		return new RGBImage(input.dimentions(), allBorders);
	}

	@Override
	public String name() {
		return "cruces por cero";
	}

}
