package ar.com.kiritsu.ati.image.entity.operation.api;

import java.io.File;

import ar.com.kiritsu.ati.image.entity.Named;

import com.google.common.base.Function;

public interface ImageTracker extends Function<File, File>, Named {

}
