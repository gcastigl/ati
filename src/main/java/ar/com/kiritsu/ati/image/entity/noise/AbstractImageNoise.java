package ar.com.kiritsu.ati.image.entity.noise;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.random.RandomGenerator;

public abstract class AbstractImageNoise implements NoiseGenerator {

	private RandomGenerator _generator;
	private String _name;

	public AbstractImageNoise(String name) {
		_name = name;
	}

	@Override
	public NoiseGenerator setRandomGenerator(RandomGenerator generator) {
		_generator = generator;
		return this;
	}

	public RandomGenerator generator() {
		return _generator;
	}

	@Override
	public String name() {
		return _name;
	}

	@Override
	public RGBImage apply(RGBImage input) {
		List<RGBColor> pixels = new ArrayList<>(input.size());
		for (RGBColor color : input.colors()) {
			float r = randomize(color.r());
			float g = randomize(color.g());
			float b = randomize(color.b());
			pixels.add(RGBColor.instance(r, g, b));
		}
		return new RGBImage(input.dimentions(), pixels);
	}

	public abstract float randomize(float n);

}
