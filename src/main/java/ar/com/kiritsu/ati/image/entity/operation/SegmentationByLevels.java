package ar.com.kiritsu.ati.image.entity.operation;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.mutable.MutableInt;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTracker;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.service.api.ImageService;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;

public class SegmentationByLevels implements ImageTracker {

	private ImageService _imageService;
	private final Vector2I _start, _end;
	
	public SegmentationByLevels(Vector2I start, Vector2I end, ImageService imageService) {
		_start = start;
		_end = end;
		_imageService = imageService;
	}

	@Override
	public String name() {
		return "segmentacion basado en conjuntos de nivel";
	}

	@Override
	public File apply(final File input) {
		String name = input.getName();
		final File out = new File(input.getParentFile().getPath() + "/tracking_" + name);
		out.mkdir();
		for (File f : out.listFiles()) {
			f.delete();
		}
		List<Point> selection = Lists.newLinkedList();
		for (int row = _start.row(); row < _end.row(); row++) {
			for (int column = _start.column(); column < _end.column(); column++) {
				selection.add(new Point(column, row));
			}
		}
		final MutableInt index = new MutableInt(37);
		tracking(selection, 
			new Supplier<RGBImage>() {
				@Override
				public RGBImage get() {
					File frame = new File(input + "/Frame" + index.intValue() + ".jpeg");
					if (frame.exists()) {
						return _imageService.build(frame);
					}
					return null;
				}
			}, 
			new Function<RGBImage, Void>() {
				@Override
				public Void apply(RGBImage input) {
					try {
						File out_ = new File(out + "/" + "out" + index.intValue() + ".png");
						ImageIO.write(input.build().get(), "png", out_);
						index.increment();
					} catch (IOException e) {
						throw new IllegalStateException(e);
					}
					return null;
				}
			});
		return input;
	}

	// Do not even ask what this method does... it just does it
	// TODO: Horrible "Consumer" simulated by Function<XXXX, Void>
	public void tracking(List<Point> selection, Supplier<RGBImage> frames, Function<RGBImage, Void> consumer) {
		RGBImage first = frames.get();
		PhiFunction tita = new PhiFunction(selection, first.height(), first.width());
		int times = (int) (1.5 * Math.max(first.height(), first.width()));
		boolean changes = true;
		List<Point> in = tita.getIn();
		@SuppressWarnings("unused")
		List<Point> out = tita.getOut();
		float[] averageIn = getAverage(first, in);
//		float[] averageOut = getAverage(first, out);
		while (times > 0 && changes) {
			RGBImage input = first != null ? first : frames.get();
			if (input == null) {
				return;
			}
			first = null;
			changes = false;
			for (Point p : tita.getlOut()) {
				if (Fd(input, p, averageIn, null)) {
					tita.setlIn(p);
					for (Point y : PhiFunction.N4(p)) {
						if (tita.isOut(y)) {
							tita.setlOut(y);
						}
					}
					for (Point y : PhiFunction.N4(p)) {
						if (tita.islIn(y)) {
							tita.setIn(y);
						}
					}
					changes = true;
				}
			}
			for (Point p : tita.getlIn()) {
				if (!Fd(input, p, averageIn, null)) {
					tita.setlOut(p);
					for (Point y : PhiFunction.N4(p)) {
						if (tita.isIn(y)) {
							tita.setlIn(y);
						}
					}
					for (Point y : PhiFunction.N4(p)) {
						if (tita.islOut(y)) {
							tita.setOut(y);
						}
					}
					changes = true;
				}
			}
			RGBImage result = input.copy();
			for (Point lout : tita.getlOut()) {
				result.colorAt(lout.y, lout.x).set(1, 0, 0);
			}
			for (Point lin : tita.getIn()) {
				result.colorAt(lin.y, lin.x).set(0, 1, 0);
			}
			consumer.apply(result);
			times--;
			System.out.println("Times: " + times);
		}
	}

	private float[] getAverage(RGBImage image, List<Point> l) {
		float[] ret = new float[] {0f, 0f, 0f};
		for (Point c : l) {
			RGBColor color = image.colorAt(c.y, c.x);
			ret[0] += color.r();
			ret[1] += color.g();
			ret[2] += color.b();
		}
		ret[0] /= l.size();
		ret[1] /= l.size();
		ret[2] /= l.size();
		return ret;
	}

	private boolean Fd(RGBImage image, Point p, float[] averageIn, float[] o) {
		double p1;
		float red, green, blue;
		RGBColor color = image.colorAt(p.y, p.x);
		
		red = color.r();
		green = color.g();
		blue = color.b();
		p1 = Math.sqrt(Math.pow((averageIn[0] - red), 2) + Math.pow((averageIn[1] - green), 2) + Math.pow((averageIn[2] - blue), 2));
//		p2 = Math.sqrt(Math.pow((averageOut[0] - red), 2) + Math.pow((averageOut[1] - green), 2) + Math.pow((averageOut[2] - blue), 2));
		return p1 < 0.3f;
	}
	
	public static class PhiFunction {

		private int[][] values;
		private int height;
		private int width;

		public PhiFunction(List<Point> selection, int height, int width) {
			values = new int[height][width];
			this.height = height;
			this.width = width;
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++) {
					values[x][y] = 3;
				}
			}
			for (Point c : selection) {
				if (!outOfBounds(c)) {
					values[c.y][c.x] = -3;
				}
			}
			List<Point> in = this.getIn();
			for (Point c : in) {
				for (Point n : N4(c)) {
					if (!outOfBounds(n)) {
						if (values[n.y][n.x] == 3) {
							values[n.y][n.x] = -1;
						}
					}
				}
			}
			List<Point> lIn = this.getlIn();
			for (Point c : lIn) {
				for (Point n : N4(c)) {
					if (!outOfBounds(n)) {
					}
					if (values[n.y][n.x] == 3) {
						values[n.y][n.x] = 1;
					}
				}
			}
		}

		public static List<Point> N4(Point p) {
			List<Point> resp = new ArrayList<Point>();
			resp.add(new Point(p.x, p.y - 1));
			resp.add(new Point(p.x - 1, p.y));
			resp.add(new Point(p.x + 1, p.y));
			resp.add(new Point(p.x, p.y + 1));
			return resp;
		}

		public boolean isOut(Point c) {
			if (outOfBounds(c))
				return false;
			return values[c.y][c.x] == 3;
		}

		private boolean outOfBounds(Point c) {
			return c.y < 0 || c.y >= this.height || c.x < 0 || c.x >= this.width;
		}

		public boolean islOut(Point c) {
			if (outOfBounds(c))
				return false;
			return values[c.y][c.x] == 1;
		}

		public boolean isIn(Point c) {
			if (outOfBounds(c))
				return false;
			return values[c.y][c.x] == -3;
		}

		public boolean islIn(Point c) {
			if (outOfBounds(c))
				return false;
			return values[c.y][c.x] == -1;
		}

		public List<Point> getlOut() {
			List<Point> ret = new ArrayList<Point>();
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++)
					if (values[x][y] == 1) {
						ret.add(new Point(y, x));
					}
			}
			return ret;
		}

		public void setlOut(Point c) {
			if (outOfBounds(c))
				return;
			values[c.y][c.x] = 1;
		}

		public List<Point> getlIn() {
			List<Point> ret = new ArrayList<Point>();
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++)
					if (values[x][y] == -1) {
						ret.add(new Point(y, x));
					}
			}
			return ret;
		}

		public void setlIn(Point c) {
			if (outOfBounds(c))
				return;
			values[c.y][c.x] = -1;
		}

		public List<Point> getIn() {
			List<Point> ret = new ArrayList<Point>();
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++)
					if (values[x][y] == -3) {
						ret.add(new Point(y, x));
					}
			}
			return ret;
		}

		public void setIn(Point c) {
			if (outOfBounds(c))
				return;
			values[c.y][c.x] = -3;
		}

		public List<Point> getInnerIn() {
			int maxX = 0, maxY = 0, minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
			List<Point> result = new ArrayList<Point>();
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++) {
					if (values[x][y] == -3) {
						if (x > maxX) {
							maxX = x;
						}
						if (y > maxY) {
							maxY = y;
						}
						if (x < minX) {
							minX = x;
						}
						if (y < minY) {
							minY = y;
						}
						result.add(new Point(y, x));
					}
				}
			}

			final int midX = (maxX + minX) / 2, midY = (maxY + minY) / 2;

			Collections.sort(result, new Comparator<Point>() {
				@Override
				public int compare(Point p1, Point p2) {
					if ((dist(p1.y, p1.x, midX, midY) - dist(p2.y, p2.x, midX, midY)) > 0) {
						return 1;
					} else {
						return -1;
					}
				}
			});

			int k = 30;
			int min = (result.size() < k) ? result.size() : k;

			result = result.subList(0, min - 1);

			return result;
		}

		private int dist(int x1, int y1, int x2, int y2) {
			return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
		}

		public List<Point> getOut() {
			List<Point> ret = new ArrayList<Point>();
			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++)
					if (values[x][y] == 3) {
						ret.add(new Point(y, x));
					}
			}
			return ret;
		}

		public void setOut(Point c) {
			if (outOfBounds(c))
				return;
			values[c.y][c.x] = 3;
		}

	}

}
