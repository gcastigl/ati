package ar.com.kiritsu.ati.image.entity.filter;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

public class GaussianFilter implements ImageTransform {

	private final GradientFilter _filter;

	public GaussianFilter(int size, float sigma) {
		final int halfSize = size / 2;
		final float sigmaSq = sigma * sigma;
		List<Float> weights = new ArrayList<Float>(size * size);
		for (int i = -halfSize; i <= halfSize; i++) {
			for (int j = -halfSize; j <= halfSize; j++) {
				float exp = -((i * i) + (j * j)) / (2 * sigmaSq);
				float n = (float) Math.pow(Math.E, exp);
				float d = (float) (2 * Math.PI * sigmaSq);
				weights.add(n / d);
			}
		}
		Optional<List<Float>> absent = Optional.absent();
		_filter = new GradientFilter(size, weights, absent);
	}

	@Override
	public RGBImage apply(RGBImage input) {
		return _filter.apply(input);
	}

	@Override
	public String name() {
		return "filtro gaussiano";
	}

}
