package ar.com.kiritsu.ati.image.entity;

public interface Named {

	String name();

}
