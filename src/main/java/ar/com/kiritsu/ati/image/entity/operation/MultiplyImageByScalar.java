package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Preconditions;

public class MultiplyImageByScalar extends PunctualImageTransform {

	public MultiplyImageByScalar(final float scalar) {
		super(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				return RGBColor.instance(
					input.color().r() * scalar, 
					input.color().g() * scalar, 
					input.color().b() * scalar);
			}
		});
		Preconditions.checkArgument(scalar >= 0);
	}
}
