package ar.com.kiritsu.ati.image.entity.tp3;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.SobelMask;
import ar.com.kiritsu.ati.image.entity.filter.GradientFilter;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class HysterisSupression implements ImageTransform {

	private final float _t1, _t2;

	private final Function<RGBImage, RGBImage> _magnitude;
	
	public HysterisSupression(float t1, float t2) {
		Preconditions.checkArgument(t1 < t2);
		_t1 = t1;
		_t2 = t2;
		final SobelMask sobelMask = new SobelMask();
		Optional<List<Float>> noList = Optional.absent();
		final ImageTransform _gx = new GradientFilter(sobelMask.maskSize(), sobelMask.xWeights(), noList);
		final ImageTransform _gy = new GradientFilter(sobelMask.maskSize(), sobelMask.yWeights(), noList);
		_magnitude = new Function<RGBImage, RGBImage>() {
			@Override
			public RGBImage apply(RGBImage input) {
				RGBImage gx = _gx.apply(input);
				RGBImage gy = _gy.apply(input);
				return new RGBImageModule().apply(Pair.of(gx, gy));
			}
		};
	}

	@Override
	public RGBImage apply(RGBImage input) {
		final RGBImage magnitude = _magnitude.apply(input);
		final int[][] borders = new int[input.height()][input.width()];
		// Marcar todos los pixels con magnitud mayor que t2 como correctos 
		// (sí pertenecen al borde) y los menores que t1 como incorrectos.
		for (int row = 0; row < borders.length; row++) {
			for (int column = 0; column < borders[row].length; column++) {
				float value = magnitude.colorAt(row, column).r();
				borders[row][column] = (value < _t1) ? -1 : ((_t2 < value) ? 1 : 0);
			}
		}
		List<RGBColor> colors = new ArrayList<>(input.size());
		// Los pixels cuya magnitud de borde está entre t1 y t2 y 
		// estan conectados con un borde, se marcan también como borde
		for (int row = 0; row < borders.length; row++) {
			for (int column = 0; column < borders[row].length; column++) {
				int value = borders[row][column]; 
				if (value == 0 && isSurroundedByAnyBorder(row, column, borders)) {
					value = 1;
				}
				colors.add((value == 1) ? RGBColor.black() : RGBColor.white());
			}
		}
		return new RGBImage(input.dimentions(), colors);
	}

	private boolean isSurroundedByAnyBorder(final int frow, final int fcolumn, int[][] values) {
		for (int drow = - 1; drow < 2; drow++) {
			int nrow = frow + drow;
			if (0 <= nrow && nrow < values.length) {
				for (int dcolumn = -1; dcolumn < 2; dcolumn++) {
					int ncolumn = fcolumn + dcolumn;
					if (0 <= ncolumn && ncolumn < values[nrow].length) {
						if (values[nrow][ncolumn] == 1) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Override	
	public String name() {
		return "Hysteris";
	}

}
