package ar.com.kiritsu.ati.image.entity.operation;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class BuildImageFromHighestColor {

	private List<RGBImage> _images = Lists.newLinkedList();

	public BuildImageFromHighestColor(List<RGBImage> images) {
		Preconditions.checkArgument(!images.isEmpty());
		_images.addAll(images);
		int pixels = images.get(0).size();
		for (int i = 1; i < images.size(); i++) {
			Preconditions.checkArgument(pixels == images.get(i).size());
		}
	}

	public RGBImage build() {
		int pixels = _images.get(0).size();
		Vector2I dimentions = _images.get(0).dimentions();
		List<RGBColor> colors = new ArrayList<>(pixels);
		for (int i = 0; i < pixels; i++) {
			float r = 0, g = 0, b = 0;
			for (int j = 0; j < _images.size(); j++) {
				RGBColor color = _images.get(j).colors().get(i);
				r = Math.max(color.r(), r);
				g = Math.max(color.g(), g);
				b = Math.max(color.b(), b);
			}
			colors.add(RGBColor.instance(r, g, b));
		}
		return new RGBImage(dimentions, colors);
	}
}
