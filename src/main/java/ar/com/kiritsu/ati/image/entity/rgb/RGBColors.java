package ar.com.kiritsu.ati.image.entity.rgb;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.operation.api.PairRGBColorTransform;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

@SuppressWarnings("serial")
public class RGBColors {

	/**
	 * Este metodo calcula el angulo entre cada canal del color y los devuelve en el rango [-180, 180] 
	 */
	private static final PairRGBColorTransform angle = new PairRGBColorTransform() {
		@Override
		public RGBColor apply(Pair<RGBColor, RGBColor> input) {
			RGBColor c1 = input.getLeft();
			RGBColor c2 = input.getRight();
			float r = (float) (Math.abs(c2.r()) < 0.001f ? 0 : Math.atan(c1.r() / c2.r()));
			float g = (float) (Math.abs(c2.g()) < 0.001f ? 0 : Math.atan(c1.g() / c2.g()));
			float b = (float) (Math.abs(c2.b()) < 0.001f ? 0 : Math.atan(c1.b() / c2.b()));
			r += Math.PI / 2;
			g += Math.PI / 2;
			b += Math.PI / 2;
			return RGBColor.instance(r, g, b);
		}
	};

	private static final PairRGBColorTransform module = new PairRGBColorTransform() {
		@Override
		public RGBColor apply(Pair<RGBColor, RGBColor> input) {
			RGBColor c1 = input.getLeft();
			RGBColor c2 = input.getRight();
			float r = (float) Math.sqrt(c1.r() * c1.r() + c2.r() * c2.r());
			float g = (float) Math.sqrt(c1.g() * c1.g() + c2.g() * c2.g());
			float b = (float) Math.sqrt(c1.b() * c1.b() + c2.b() * c2.b());
			return RGBColor.instance(r, g, b);
		}
	};

	private static final PairRGBColorTransform addition = new PairRGBColorTransform() {
		@Override
		public RGBColor apply(Pair<RGBColor, RGBColor> input) {
			RGBColor c1 = input.getLeft();
			RGBColor c2 = input.getRight();
			return RGBColor.instance(c1.r() + c2.r(), c1.g() + c2.g(), c1.b() + c2.b());
		}
	};

	private static final PairRGBColorTransform substraction = new PairRGBColorTransform() {
		@Override
		public RGBColor apply(Pair<RGBColor, RGBColor> input) {
			RGBColor c1 = input.getLeft();
			RGBColor c2 = input.getRight();
			return RGBColor.instance(c1.r() - c2.r(), c1.g() - c2.g(), c1.b() - c2.b());
		}
	};

	private static final PairRGBColorTransform multiplication = new PairRGBColorTransform() {
		@Override
		public RGBColor apply(Pair<RGBColor, RGBColor> input) {
			RGBColor c1 = input.getLeft();
			RGBColor c2 = input.getRight();
			return RGBColor.instance(c1.r() * c2.r(), c1.g() * c2.g(), c1.b() * c2.b());
		}
	};

	private static final Predicate<RGBColor> needsCorrection = new Predicate<RGBColor>() {
		@Override
		public boolean apply(RGBColor input) {
			return !withinLimits(input.r()) || !withinLimits(input.g()) || !withinLimits(input.b());
		}

		private boolean withinLimits(float x) {
			return 0 <= x && x <= 1;
		}
	};

	public static final Function<Integer, RGBColor> unpacked() {
		return new Function<Integer, RGBColor>() {
			public RGBColor apply(Integer input) {
				return RGBColor.unpack(input);
			}
		};
	}

	public static final Function<RGBColor, Integer> packed() {
		return new Function<RGBColor, Integer>() {
			public Integer apply(RGBColor input) {
				return input.pack();
			}
		};
	}

	public static final PairRGBColorTransform module() {
		return module;
	}

	public static final PairRGBColorTransform angle() {
		return angle;
	}

	public static final PairRGBColorTransform addition() {
		return addition;
	}

	public static final PairRGBColorTransform substraction() {
		return substraction;
	}

	public static final PairRGBColorTransform multiplication() {
		return multiplication;
	}

	public static final Predicate<RGBColor> needsCorrection() {
		return needsCorrection;
	}
	
	public static final PairRGBColorTransform max() {
		return new PairRGBColorTransform() {
			@Override
			public RGBColor apply(Pair<RGBColor, RGBColor> input) {
				RGBColor c1 = input.getLeft();
				RGBColor c2 = input.getRight();
				float r = Math.max(c1.r(), c2.r());
				float g = Math.max(c1.g(), c2.g());
				float b = Math.max(c1.b(), c2.b());
				return RGBColor.instance(r, g, b);
			}
		};
	}
	
	public static Function<RGBColor, Float> r() {
		return new Function<RGBColor, Float>() {
			@Override
			public Float apply(RGBColor input) {
				return input.r();
			}
		};
	}
	
	public static Function<RGBColor, Float> g() {
		return new Function<RGBColor, Float>() {
			@Override
			public Float apply(RGBColor input) {
				return input.g();
			}
		};
	}
	
	public static Function<RGBColor, Float> b() {
		return new Function<RGBColor, Float>() {
			@Override
			public Float apply(RGBColor input) {
				return input.b();
			}
		};
	}
}
