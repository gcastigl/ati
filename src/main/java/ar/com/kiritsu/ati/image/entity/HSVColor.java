package ar.com.kiritsu.ati.image.entity;

public class HSVColor {

	private float _h;
	private float _s;
	private float _v;

	public HSVColor(float h, float s, float v) {
		h(h);
		s(s);
		v(v);
	}

	public void h(float h) {
		_h = h;
	}

	public void s(float s) {
		_s = s;
	}

	public void v(float v) {
		_v = v;
	}

	public float h() {
		return _h;
	}

	public float s() {
		return _s;
	}

	public float v() {
		return _v;
	}
}
