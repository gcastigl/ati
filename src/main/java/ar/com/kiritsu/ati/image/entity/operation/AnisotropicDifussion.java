package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.filter.FindNeighbors;
import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;

public class AnisotropicDifussion implements ImageTransform {

	private static final RGBColor DEFAULT_COLOR = RGBColor.black();

	private Function<Float, Float> _g;

	public AnisotropicDifussion(Function<Float, Float> g) {
		_g = g;
	}

	@Override
	public RGBImage apply(RGBImage input) {
		final FindNeighbors findNeighbors = new FindNeighbors(input, 3);
		return new PunctualImageTransform(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				Vector2I position = input.position();
				PixelNeighbors neighbors = findNeighbors.at(position.row(), position.column());
				RGBColor middle = input.color();
				RGBColor result = new RGBColor();
				addToResult(getNieghbor(neighbors, -1, 0).substract(middle), result);
				addToResult(getNieghbor(neighbors, 1, 0).substract(middle), result);
				addToResult(getNieghbor(neighbors, 0, -1).substract(middle), result);
				addToResult(getNieghbor(neighbors, 0, 1).substract(middle), result);
				return result.scale(1/4f).add(middle);
			}
		}).apply(input);
	}

	private RGBColor getNieghbor(PixelNeighbors neighbors, int row, int column) {
		return new RGBColor(neighbors.get(row, column).or(DEFAULT_COLOR));
	}

	private void addToResult(RGBColor dn, RGBColor result) {
		result.addR(dn.r() * _g.apply(dn.r()));
		result.addG(dn.g() * _g.apply(dn.g()));
		result.addB(dn.b() * _g.apply(dn.b()));
	}

	@Override
	public String name() {
		return "Difusion anisotropica";
	}

}
