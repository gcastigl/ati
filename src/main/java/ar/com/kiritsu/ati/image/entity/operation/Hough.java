package ar.com.kiritsu.ati.image.entity.operation;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

//{
//	cmd=hough,
//	t=0.1,
//	type=R,
//	dr=1,
//	da=1,
//	eps=0.8,
//	votosMin=105
//}
public class Hough implements ImageTransform {

	public static enum DetectionType {LINE, CIRCLE};
	
	private ImageTransform _detectBorders;
	private Function<RGBImage, RGBImage> _thresshold;
	private Function<RGBImage, ParametricSpace> _parametricSpaceBuilder;
	private int _minVotes;

	public Hough(float susant, final float dr, final float da, final float epsilon, int minVotes, final DetectionType detectionType) {
		Optional<Float> none = Optional.of(0.1f);
		_detectBorders = new Susan(susant, false, none);
		_thresshold = new OtsuImageThresholding();
//		_thresshold = Functions.identity();
		_minVotes = minVotes;
		_parametricSpaceBuilder = new Function<RGBImage, Hough.ParametricSpace>() {
			@Override
			public ParametricSpace apply(RGBImage input) {
				return detectionType.equals(DetectionType.LINE) ?
					new LineParametricSpace(input.dimentions(), dr, da, epsilon) : 
					new CircularParametricSpace(input.dimentions(), dr, da, epsilon);
			}
		};
	}

	@Override
	public RGBImage apply(RGBImage input) {
		RGBImage bordered = _detectBorders.apply(input);
		RGBImage umbralized = _thresshold.apply(bordered);
		ParametricSpace parametricSpace = _parametricSpaceBuilder.apply(umbralized);
		for (int row = 0; row < input.height(); row++) {
			for (int column = 0; column < input.width(); column++) {
				float c = umbralized.colorAt(row, column).r();
				if (c == 1) {
					parametricSpace.countVotes(row, column);
				}
			}
		}
		parametricSpace.drawVotes(umbralized);
		return umbralized;
	}

	@Override
	public String name() {
		return "Hough";
	}

	private interface ParametricSpace {
	
		void countVotes(int row, int column);

		void drawVotes(RGBImage image);

	}

	private class LineParametricSpace implements ParametricSpace {

		private int[][] _A;
		private List<Float> _rs, _angles;
		private float _e;

		public LineParametricSpace(Vector2I dim, float dr, float da, float e) {
			_e = e;
			float D = (float) (Math.max(dim.row(), dim.column()) * Math.sqrt(2));
			float minr = -D;
			float maxr = D;
			int rsCount = (int) ((maxr - minr) / dr);
			_rs = new ArrayList<>(rsCount);
			for (int i = 0; i <= rsCount; i++) {
				_rs.add(minr + i * dr);
			}
			int anglesCount = (int) (180 / da);
			_angles = new ArrayList<>(anglesCount);
			for (int i = 0; i < anglesCount; i++) {
				_angles.add((float) Math.toRadians(-90 + i * da));
			}
			_A = new int[_rs.size()][_angles.size()];
		}

		@Override
		public void countVotes(int row, int column) {
			int aIndex = 0;
			for (float a : _angles) {
				float pr = (float) (column * cos(a) + row * sin(a));
				int rIndex = 0;
				for (float r : _rs) {
					if (Math.abs(r - pr) < _e) {
						_A[rIndex][aIndex]++;
//						String s = "[" + row + ", " + column + "] = " + _A[rIndex][aIndex];
//						s += "| r = " + r + ", a = " + Math.toDegrees(a);
//						System.out.println(s);
//						System.out.print("");
					}
					rIndex++;
				}
				aIndex++;
			}
		}
		
		@Override
		public void drawVotes(RGBImage image) {
			RGBColor red = new RGBColor(1,  0,  0);
			int aIndex = 0;
			List<Float> rs = Lists.newLinkedList();
			List<Float> as = Lists.newLinkedList();
			for (float a : _angles) {
				int rIndex = 0;
				for (float r : _rs) {
					if (_A[rIndex][aIndex] >= _minVotes) {
						rs.add(r);
						as.add((float) Math.toDegrees(a));
						if (Math.abs(a) < 0.1) {
							for (int row = 0; row < image.height(); row++) {
								image.set(row, (int) r, red);
							}
						} else {
							for (int column = 0; column < image.width(); column++) {
								int row = (int) ((r - column * cos(a)) / sin(a));
								image.setSafe(row, column, red);
							}
						}
					}
					rIndex++;
				}
				aIndex++;
			}
		}
	}

	private class CircularParametricSpace implements ParametricSpace {

		private final int[][][] _A;
		private List<Float> _xs = Lists.newArrayList();
		private List<Float> _ys = Lists.newArrayList();
		private List<Float> _rs = Lists.newArrayList();
		private float _eps;

		public CircularParametricSpace(Vector2I dim, float dx, float dr, float eps) {
			_eps = eps;
			float D = (float) (Math.max(dim.row(), dim.column()) * Math.sqrt(2));
			float minxy = -D;
			float maxxy = D;
			int xysCount = (int) ((maxxy - minxy) / dr);
			for (int i = 0; i < xysCount; i++) {
				_xs.add(minxy + i * dr);
				_ys.add(minxy + i * dr);
			}
			for (int i = 0; i < 200; i++) {
				_rs.add(i * 2f);
			}
			_A = new int[_xs.size()][_ys.size()][_rs.size()];
		}
		
		@Override
		public void countVotes(int row, int column) {
			int xIndex = 0;
			for (float x : _xs) {
				int yIndex = 0;
				for (float y : _ys) {
					int rIndex = 0;
					for (float r : _rs) {
						float dx = column - x;
						float dy = row - y;
						float c = dx * dx + dy * dy;
						if (Math.abs(c - r) < _eps) {
							_A[xIndex][yIndex][rIndex]++;
						}
						rIndex++;
					}
					yIndex++;
				}
				xIndex++;
			}
		}
		
		@Override
		public void drawVotes(RGBImage image) {
			RGBColor mark = new RGBColor(1, 0, 0);
			int xIndex = 0;
			for (float x : _xs) {
				int yIndex = 0;
				for (float y : _ys) {
					int rIndex = 0;
					for (float r : _rs) {
						if (_A[xIndex][yIndex][rIndex] > _minVotes) {
							int x1 = (int) (x + r); int y1 = (int) (y);
							int x2 = (int) (x - r); int y2 = (int) (y);
							int x3 = (int) (x); 	int y3 = (int) (y + r);
							int x4 = (int) (x); 	int y4 = (int) (y - r);
							image.setSafe(x1, y1, mark);
							image.setSafe(x2, y2, mark);
							image.setSafe(x3, y3, mark);
							image.setSafe(x4, y4, mark);
						}
						rIndex++;
					}
					yIndex++;
				}
				xIndex++;
			}
		}
	}
}
