package ar.com.kiritsu.ati.image.entity.filter;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

public class ImageFilterMasks {

	public static final List<Float> averageMask(int size) {
		Preconditions.checkArgument(size > 0 && size % 2 == 1);
		int cells = size * size;
		List<Float> weights = new ArrayList<>(cells);
		for (int i = 0; i < cells; i++) {
			weights.add(1f / cells);
		}
		setZeroAtCenter(weights, size);
		return weights;
	}

	private static final void setZeroAtCenter(List<Float> weights, int size) {
		int center = (size / 2);
		weights.set(center * size + center, 0f);
	}

	public static final List<Float> borderEnhancement() {
		int size = 3;
		List<Float> weights = new ArrayList<>(size * size);
		weights.add(-1f / 9);
		weights.add(-1f / 9);
		weights.add(-1f / 9);
		weights.add(-1f / 9);
		weights.add(8f / 9);
		weights.add(-1f / 9);
		weights.add(-1f / 9);
		weights.add(-1f / 9);
		weights.add(-1f / 9);
		return weights;
	}
}
