package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;

public class ContrastImprovement extends PunctualImageTransform {

	private float _r1, _r2;
	private Function<Float, Float> _f1, _f2, _f3;

	public ContrastImprovement(Function<Float, Float> f1, Function<Float, Float> f2, Function<Float, Float> f3, float r1, float r2) {
		_f1 = f1;
		_f2 = f2;
		_f3 = f3;
		Preconditions.checkArgument(0 <= r1 && r1 < 1);
		Preconditions.checkArgument(r1 <= r2 && r2 <= 1);
		_r1 = r1;
		_r2 = r2;
		setTransformation(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				float r = improve(input.color().r());
				float g = improve(input.color().g());
				float b = improve(input.color().b());
				return RGBColor.instance(r, g, b);
			}
		});
	}

	private float improve(float n) {
		Function<Float, Float> f;
		if (n <= _r1) {
			f = _f1;
		} else if (n <= _r2) {
			f = _f2;
		} else {
			f = _f3;
		}
		return f.apply(n);
	}
}
