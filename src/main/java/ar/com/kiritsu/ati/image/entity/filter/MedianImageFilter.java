package ar.com.kiritsu.ati.image.entity.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors.ColorAndMaskPosition;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.collect.Iterables;

public class MedianImageFilter extends ImageConvolution {

	private static final RGBColor DEFAULT_COLOR = RGBColor.black();
	private static final Comparator<Float> ascending = new FloatComprator();

	private List<Float> _reds, _green, _blues;
	private boolean isPair;

	public MedianImageFilter(int windowSideLen) {
		super(windowSideLen);
		int neighbors = windowSideLen() * windowSideLen();
		isPair = neighbors % 2 == 0;
		_reds = new ArrayList<>(neighbors);
		_green = new ArrayList<>(neighbors);
		_blues = new ArrayList<>(neighbors);
	}

	@Override
	public String name() {
		return "Filtro de la mediana";
	}

	@Override
	public RGBColor getColor(PixelNeighbors neighbors) {
		_reds.clear();
		_blues.clear();
		_green.clear();
		for (ColorAndMaskPosition colorAndPos : neighbors.valuesWithMiddle()) {
			_reds.add(colorAndPos.color().or(DEFAULT_COLOR).r());
			_green.add(colorAndPos.color().or(DEFAULT_COLOR).g());
			_blues.add(colorAndPos.color().or(DEFAULT_COLOR).b());
		}
		return RGBColor.instance(mean(_reds), mean(_green), mean(_blues));
	}

	private float mean(List<Float> values) {
		Collections.sort(values, ascending);
		int half = values.size() / 2;
		if (isPair) {
			float sum = Iterables.get(values, half) + Iterables.get(values, half + 1);
			return sum / 2;
		} else {
			return Iterables.get(values, half + 1);
		}
	}

	private static final class FloatComprator implements Comparator<Float> {
		@Override
		public int compare(Float o1, Float o2) {
			return o1.compareTo(o2);
		}
	}
}
