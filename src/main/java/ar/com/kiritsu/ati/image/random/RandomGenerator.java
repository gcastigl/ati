package ar.com.kiritsu.ati.image.random;

public interface RandomGenerator {

	float get();

}
