package ar.com.kiritsu.ati.image.entity.tp3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.SobelMask;
import ar.com.kiritsu.ati.image.entity.filter.GradientFilter;
import ar.com.kiritsu.ati.image.entity.filter.ImageConvolution;
import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

/*
 * Canny: 
 * 	Gauss
 * 	Sobel
 * 	Supresion no maximos { lo que esta aqui }
 * 	Hysteriss
 */
public class NonMaximumSuppression implements ImageTransform {

	private static final Map<Integer, int[][]> DIRECTIONS;
	static {
		DIRECTIONS = Maps.newHashMap();
		DIRECTIONS.put(0, new int[][] { { 0, -1 }, { 0, 1 } });
		DIRECTIONS.put(45, new int[][] { { -1, -1 }, { 1, 1 } });
		DIRECTIONS.put(90, new int[][] { { -1, 0 }, { 1, 0 } });
		DIRECTIONS.put(135, new int[][] { { -1, 1 }, { 1, -1 } });
	}

	private Function<RGBImage, RGBImage> _gx, _gy;

	public NonMaximumSuppression() {
		SobelMask sobelMask = new SobelMask();
		Optional<List<Float>> noList = Optional.absent();
		_gx = new GradientFilter(sobelMask.maskSize(), sobelMask.xWeights(), noList);
		_gy = new GradientFilter(sobelMask.maskSize(), sobelMask.yWeights(), noList);
	}

	@Override
	public RGBImage apply(RGBImage input) {
		RGBImage gx = _gx.apply(input);
		RGBImage gy = _gy.apply(input);
		RGBImage mod = new RGBImageModule().apply(Pair.of(gx, gy));
		final RGBImage angleImage = new RGBImageAngle().apply(Pair.of(gy, gx));
		final MutableInt index = new MutableInt();
		final Mutable<FileWriter> wrter = new MutableObject<>();
		try {
			wrter.setValue(new FileWriter(new File("text.txt")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ImageConvolution(3) {
			@Override
			public RGBColor getColor(PixelNeighbors neighbors) {
				RGBColor c = neighbors.get(0, 0).get();
				float angle = angleImage.colors().get(index.getValue()).r();
				index.increment();
				int[][] d = DIRECTIONS.get(getDirection(angle));
				try {
					wrter.getValue().write(Math.toDegrees(angle) + " => " + d[0][0] + ", " + d[0][1] + " => " + d[1][0] + ", " + d[1][1] + "\n");
					wrter.getValue().flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				RGBColor c1 = neighbors.get(d[0][0], d[0][1]).or(RGBColor.black());
				RGBColor c2 = neighbors.get(d[1][0], d[1][1]).or(RGBColor.black());
				if (c.r() < c1.r() || c.r() < c2.r()) {
					return RGBColor.black();
				}
				return new RGBColor(c.r(), c.r(), c.r());
			}

			private int getDirection(float angle) {
				angle = (float) Math.toDegrees(angle);
				Preconditions.checkArgument(0 <= angle && angle <= 180, angle);
				if (angle <= 22.5f || 157.5 <= angle) {
					return 0;
				}
				if (22.5f < angle && angle <= 67.5f) {
					return 45;
				}
				if (67.5f < angle && angle <= 112.5f) {
					return 90;
				}
				if (112.5f < angle && angle <= 157.5f) {
					return 135;
				}
				throw new IllegalStateException(angle + " is invalid");
			}
		}.apply(mod);
	}

	@Override
	public String name() {
		return "Supresion de no maximos";
	}
}
