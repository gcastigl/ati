package ar.com.kiritsu.ati.image.entity.filter;

import java.util.List;

import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors.ColorAndMaskPosition;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Preconditions;

public class AdditiveWeightenedFilter extends ImageConvolution {

	private final static RGBColor DEFAULT_COLOR = RGBColor.black();

	private final List<Float> _weights;

	public AdditiveWeightenedFilter(int windowSideLen, List<Float> weights) {
		super(windowSideLen);
		Preconditions.checkArgument(weights.size() == windowSideLen * windowSideLen);
		_weights = weights;
	}

	@Override
	public String name() {
		return "Filtrado ponderado (suma)";
	}

	@Override
	public RGBColor getColor(PixelNeighbors neighbors) {
		return multiply(neighbors.valuesWithMiddle());
	}

	public RGBColor multiply(List<ColorAndMaskPosition> neighbors) {
		int size = neighbors.size();
		float r = 0, g = 0, b = 0;
		for (int i = 0; i < size; i++) {
			RGBColor color = neighbors.get(i).color().or(DEFAULT_COLOR);
			float w = _weights.get(i);
			r += w * color.r();
			g += w * color.g();
			b += w * color.b();
		}
		return RGBColor.instance(r, g, b);
	}	
}
