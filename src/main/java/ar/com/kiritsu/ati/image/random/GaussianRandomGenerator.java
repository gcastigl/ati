package ar.com.kiritsu.ati.image.random;

import java.util.Random;

import com.google.common.base.Preconditions;

public class GaussianRandomGenerator implements RandomGenerator {

	private Random random = new Random();
	private float _median, _std;

	public GaussianRandomGenerator(float median, float std) {
		Preconditions.checkArgument(std > 0);
		_median = median;
		_std = std;
	}

	@Override
	public float get() {
		float normalGuassian = (float) random.nextGaussian();
		return normalGuassian * _std + _median;
	}
}
