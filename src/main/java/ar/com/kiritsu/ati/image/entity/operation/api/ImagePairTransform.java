package ar.com.kiritsu.ati.image.entity.operation.api;

import java.io.Serializable;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;

public interface ImagePairTransform extends Function<Pair<RGBImage, RGBImage>, RGBImage>, Serializable {

	String getName();

}
