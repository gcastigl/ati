package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Preconditions;

public class ToBinaryImage extends PunctualImageTransform {

	public ToBinaryImage(final float threshold) {
		super(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				float r = applyThresshold(input.color().r());
				float g = applyThresshold(input.color().g());
				float b = applyThresshold(input.color().b());
				return RGBColor.instance(r, g, b);
			}

			private float applyThresshold(float n) {
				return n < threshold ? 0 : 1;
			}
		});
		Preconditions.checkArgument(0 <= threshold && threshold <= 1);
	}

}
