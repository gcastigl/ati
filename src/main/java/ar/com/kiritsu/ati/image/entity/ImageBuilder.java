package ar.com.kiritsu.ati.image.entity;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;

public class ImageBuilder {

	private Function<Vector2I, RGBColor> _color;
	
	public ImageBuilder(Function<Vector2I, RGBColor> color) {
		_color = color;
	}
	
	public RGBImage build(int width, int height) {
		List<RGBColor> colors = new ArrayList<>(width * height);
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				colors.add(_color.apply(new Vector2I(row, column)));
			}
		}
		return new RGBImage(width, height, colors);
	}

}
