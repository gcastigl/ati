package ar.com.kiritsu.ati.image.entity.rgb;

import org.apache.commons.lang3.tuple.MutablePair;

public class RGBImageGradient {

	private final MutablePair<RGBColor, RGBColor> tuple = new MutablePair<>();

	public RGBImageGradient(RGBImage image) {
//		List<RGBColor> gradients = new ArrayList<>(image.size());
//		for (int row = 0; row < image.height(); row++) {
//			for (int column = 0; column < image.width(); column++) {
//				RGBColor color = image.colorAt(row, column);
//				RGBColor gradient;
//				if (column == 0) {
//					RGBColor next = image.colorAt(row, column + 1);
//					gradient = new RGBColor(next).substract(color.r(), color.g(), color.b());
//				} else if (column == image.width()) {
//					RGBColor prev = image.colorAt(row, column - 1);
//					gradient = new RGBColor(color).substract(prev.r(), prev.g(), prev.b());
//				} else {
//					RGBColor next = image.colorAt(row, column + 1);
//					RGBColor prev = image.colorAt(row, column - 1);
//					gradient = new RGBColor(next).substract(prev.r(), prev.g(), prev.b()).scale(0.5f);
//				}
//				gradients.add(gradient);
//			}
//		}
	}

	public RGBColor at(RGBColor c1, RGBColor c2) {
		tuple.setLeft(c1);
		tuple.setRight(c2);
		return RGBColors.substraction().apply(tuple);
	}
}
