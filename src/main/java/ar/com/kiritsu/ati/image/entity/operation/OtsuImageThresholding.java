package ar.com.kiritsu.ati.image.entity.operation;

import static ar.com.kiritsu.ati.image.entity.rgb.RGBColors.r;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;

public class OtsuImageThresholding implements ImageTransform {

	@Override
	public RGBImage apply(RGBImage input) {
		Function<RGBColor, Float> colorBand = r();
		float[] pis = pi(input, colorBand);
		float maxSigmaSq = 0;
		int tForMaxSigmaSq = 0;
		for (int t = 0; t <= 255; t++) {
			float[] ws = calcWs(pis, t);
			float[] mus = calcMus(pis, ws, t);
			float ut = ws[0] * mus[0] + ws[1] * mus[1];
			float m1 = mus[0] - ut;
			float m2 = mus[1] - ut;
			float sigmaSq = ws[0] * m1 * m1 + ws[1] * m2 * m2;
			if (maxSigmaSq <= sigmaSq) {
				maxSigmaSq = sigmaSq;
				tForMaxSigmaSq = t;
			}
		}
		List<RGBColor> colors = new ArrayList<>(input.size());
		for (RGBColor color : input.colors()) {
			float band = colorBand.apply(color);
			if (band * 255 < tForMaxSigmaSq) {
				colors.add(RGBColor.black());
			} else {
				colors.add(RGBColor.white());
			}
		}
		return new RGBImage(input.dimentions(), colors);
	}

	private float[] pi(RGBImage image, Function<RGBColor, Float> colorBand) {
		float[] pi = new float[256];
		for (int i = 0; i < image.size(); i++) {
			int I = (int) (colorBand.apply(image.colors().get(i)) * 255);
			pi[I]++;
		}
		for (int i = 0; i < pi.length; i++) {
			pi[i] /= image.size();
		}
		return pi;
	}

	private float[] calcWs(float[] pis, final int t) {
		float w1 = 0, w2 = 0;
		for (int t1 = 1; t1 <= t; t1++) {
			w1 += pis[t1];
		}
		for (int t2 = t + 1; t2 <= 255; t2++) {
			w2 += pis[t2];
		}
		return new float[] { w1, w2 };
	}

	private float[] calcMus(final float[] pis, final float[] ws, final int t) {
		float u1 = 0, u2 = 0;
		for (int t1 = 1; t1 <= t; t1++) {
			u1 += t1 * pis[t1] / ws[0];
		}
		for (int t2 = t + 1; t2 <= 255; t2++) {
			u2 += t2 * pis[t2] / ws[1];
		}
		return new float[] { u1, u2 };
	}

	@Override
	public String name() {
		return "Umbralizacion Otsu";
	}

}
