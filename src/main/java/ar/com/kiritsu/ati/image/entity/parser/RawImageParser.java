package ar.com.kiritsu.ati.image.entity.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.RawImage;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Optional;

public class RawImageParser implements ImageFileParser {

	private final Map<String, Vector2I> _dimentions;

	public RawImageParser(Map<String, Vector2I> dimentions) {
		_dimentions = dimentions;
	}

	@Override
	public Optional<RGBImage> apply(File input) {
		Vector2I dimentions = _dimentions.get(input.getName());
		if (dimentions == null) {
			System.out.println("Unexpected image: " + input);
			return Optional.absent();
		}
		int pixelCount = dimentions.x() * dimentions.y();
		if (pixelCount != input.length()) {
			return Optional.absent();
		}
		RawImage image = new RawImage(input, dimentions.x(), dimentions.y());
		try {
			List<RGBColor> pixels = new ArrayList<>(pixelCount);
			for (Integer color : image.getBytes()) {
				pixels.add(RGBColor.fromGrayScale(color));
			}
			return Optional.of(new RGBImage(dimentions.y(), dimentions.x(), pixels));
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.absent();
		}
	}

}
