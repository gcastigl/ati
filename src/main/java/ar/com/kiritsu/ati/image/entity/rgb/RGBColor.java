package ar.com.kiritsu.ati.image.entity.rgb;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.google.common.base.Optional;

@SuppressWarnings("serial")
public class RGBColor implements Serializable {

	public static Optional<RGBColor> absent() {
		return Optional.absent();
	}

	public static final int DEPTH = 256;

	private static final float OPAQUE = 1;
	@SuppressWarnings("unused")
	private static final float TRANSPARENT = 0;

	private static final RGBColor WHITE 	= new RGBColor(1, 1, 1);
	private static final RGBColor BLACK 	= new RGBColor(0, 0, 0);

	public static RGBColor white() {
		return new RGBColor(1, 1, 1);
	}
	
	public static RGBColor black() {
		return new RGBColor(0, 0, 0);
	}
	
	public static final RGBColor instance(float r, float g, float b) {
		return new RGBColor(r, g, b);
	}

	public static final float unpackAlpha(int value) {
		int a = (value & 0xFF000000) >> 24;
		if (a < 0) {
			a += 256;
		}
		if (a == 0) {
			a = 255;
		}
		return a / 255f;
	}

	public static final float unpackRed(int value) {
		return ((value & 0x00FF0000) >> 16)  / 255f;
	}

	public static final float unpackGreen(int value) {
		return ((value & 0x0000FF00) >> 8)  / 255f;
	}

	public static final float unpackBlue(int value) {
		return (value & 0x000000FF)  / 255f;
	}

	public static RGBColor unpack(int value) {
		float r = unpackRed(value);
		float g = unpackGreen(value);
		float b = unpackBlue(value);
		return new RGBColor(r, g, b);
	}

	public static final RGBColor fromGrayScale(int b) {
		float color = b / 255f;
		if (color == 0) {
			return BLACK;
		} else if (color == 1) {
			return WHITE;
		}
		return new RGBColor(color, color, color);
	}

	private float _r;
	private float _g;
	private float _b;

	public RGBColor() {
		this(0, 0, 0);
	}

	public RGBColor(RGBColor other) {
		this(other.r(), other.g(), other.b());
	}

	public RGBColor(float r, float g, float b) {
		r(r);
		g(g);
		b(b);
	}

	public float r() {
		return _r;
	}

	public float g() {
		return _g;
	}

	public float b() {
		return _b;
	}

	public float a() {
		return OPAQUE;
	}

	public void r(float r) {
		_r = r;
	}

	public void addR(float r) {
		r(r() + r);
	}

	public void g(float g) {
		_g = g;
	}

	public void addG(float g) {
		g(g() + g);
	}

	public void b(float b) {
		_b = b;
	}

	public void addB(float b) {
		b(b() + b);
	}

	public RGBColor substract(RGBColor other) {
		return substract(other.r(), other.g(), other.b());
	}

	public RGBColor substract(float r, float g, float b) {
		add(-r, -g, -b);
		return this;
	}

	public RGBColor add(RGBColor color) {
		return add(color.r(), color.g(), color.b());
	}

	public RGBColor add(float r, float g, float b) {
		addR(r);
		addG(g);
		addB(b);
		return this;
	}

	public int redByte() {
		return (int) (r() * 255);
	}

	public int greenByte() {
		return (int) (g() * 255);
	}

	public int blueByte() {
		return (int) (b() * 255);
	}

	public int alphaByte() {
		return (int) (a() * 255);
	}

	public void set(RGBColor color) {
		set(color.r(), color.g(), color.b());
	}

	public void set(float r, float g, float b) {
		r(r);
		g(g);
		b(b);
	}

	public RGBColor scale(float k) {
		set(r() * k, g() * k, b() * k);
		return this;
	}

	public int pack() {
		return alphaByte() << 24 | redByte() << 16 | greenByte() << 8 | blueByte();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(r()).append(g()).append(b()).append(a())
			.build();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof RGBColor)) {
			return false;
		}
		RGBColor other = (RGBColor) obj;
		return new EqualsBuilder()
			.append(r(), other.r())
			.append(g(), other.g())
			.append(b(), other.b())
			.append(a(), other.a())
			.isEquals();
	}

	@Override
	public String toString() {
		return "[" + r() + ", " + g() + ", " + b() + "]";
	}
}
