package ar.com.kiritsu.ati.image.entity.filter;

import static ar.com.kiritsu.ati.image.entity.MathUtil.norm2;

import java.util.List;

import ar.com.kiritsu.ati.image.entity.filter.PixelNeighbors.ColorAndMaskPosition;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class GradientFilter extends ImageConvolution {

	private static final RGBColor DEFAULT_COLOR = RGBColor.black();

	private List<Float> _xWeights;
	private Optional<List<Float>> _yWeights;

	public GradientFilter(int windowSideLen, List<Float> xWeights, Optional<List<Float>> yWeights) {
		super(windowSideLen);
		Preconditions.checkArgument(xWeights.size() == windowSideLen * windowSideLen);
		if (yWeights.isPresent()) {
			Preconditions.checkArgument(yWeights.get().size() == windowSideLen * windowSideLen);
		}
		_xWeights = xWeights;
		_yWeights = yWeights;
	}

	@Override
	public String name() {
		return "Operador de gradiente";
	}

	@Override
	public RGBColor getColor(PixelNeighbors neighbors) {
		RGBColor xResult = new RGBColor();
		RGBColor yResult = new RGBColor();
		List<ColorAndMaskPosition> colorAndPoss = neighbors.valuesWithMiddle();
		int size = neighbors.totalSize();
		for (int i = 0; i < size; i++) {
			multiply(colorAndPoss.get(i).color().or(DEFAULT_COLOR), _xWeights.get(i), xResult);
			if (_yWeights.isPresent()) {
				multiply(colorAndPoss.get(i).color().or(DEFAULT_COLOR), _yWeights.get().get(i), yResult);
			}
		}
		float r, g, b;
		if (_yWeights.isPresent()) {
			r = norm2(xResult.r(), yResult.r());
			g = norm2(xResult.g(), yResult.g());
			b = norm2(xResult.b(), yResult.b());
		} else {
			r = xResult.r();
			g = xResult.g();
			b = xResult.b();
		}
		return RGBColor.instance(r, g, b);
	}

	public void multiply(RGBColor color, float w, RGBColor output) {
		output.addR(w * color.r());
		output.addG(w * color.g());
		output.addB(w * color.b());
	}

}
