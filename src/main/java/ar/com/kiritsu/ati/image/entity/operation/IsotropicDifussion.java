package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.util.FloatFunctions;

public class IsotropicDifussion implements ImageTransform {

	@Override
	public RGBImage apply(RGBImage input) {
		return new AnisotropicDifussion(FloatFunctions.one()).apply(input);
	}

	@Override
	public String name() {
		return "Difusion isotropica";
	}
}
