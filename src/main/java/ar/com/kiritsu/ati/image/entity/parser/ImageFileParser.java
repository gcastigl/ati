package ar.com.kiritsu.ati.image.entity.parser;

import java.io.File;

import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;

public interface ImageFileParser extends Function<File, Optional<RGBImage>> {

}
