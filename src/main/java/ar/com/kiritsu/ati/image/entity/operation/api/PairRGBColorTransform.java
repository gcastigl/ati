package ar.com.kiritsu.ati.image.entity.operation.api;

import java.io.Serializable;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Function;

public interface PairRGBColorTransform extends Function<Pair<RGBColor, RGBColor>, RGBColor>, Serializable {

}
