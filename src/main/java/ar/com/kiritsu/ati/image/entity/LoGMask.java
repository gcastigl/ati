package ar.com.kiritsu.ati.image.entity;

import java.util.ArrayList;
import java.util.List;

public class LoGMask {

	private final float _sigma;
	private final List<Float> _mask = new ArrayList<>();

	public LoGMask(int windowSideLen, float sigma) {
		_sigma = sigma;
		int start = -windowSideLen / 2;
		int end = windowSideLen / 2;
		for (int i = start; i <= end; i++) {
			for (int j = start; j <= end; j++) {
				_mask.add(log(i, j));
			}
		}
	}

	public List<Float> mask() {
		return _mask;
	}

	private float log(int x, int y) {
		float sigmaSq = _sigma * _sigma;
		float xSq = x * x;
		float ySq = y * y;
		float n = -(xSq + ySq) / sigmaSq;
		float c = -(2 + n) / (float) (Math.sqrt(2 * Math.PI) * sigmaSq * _sigma);
		return c * (float) Math.pow(Math.E, n / 2);
	}
}
