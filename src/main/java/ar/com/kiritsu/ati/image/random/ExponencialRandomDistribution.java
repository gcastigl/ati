package ar.com.kiritsu.ati.image.random;

public class ExponencialRandomDistribution implements RandomGenerator {

	private float _mean;
	
	public ExponencialRandomDistribution(float mean) {
		_mean = mean;
	}

	@Override
	public float get() {
		float x = random(0.01f, 1);
		float random = ((float) - Math.log(x)) / _mean;
		return random;
	}

	private float random(float min, float max) {
		float x = (float) Math.random();
		return x * (max - min) + min;
	}
}
