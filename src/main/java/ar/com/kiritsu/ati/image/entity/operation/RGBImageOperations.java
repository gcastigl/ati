package ar.com.kiritsu.ati.image.entity.operation;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;

public class RGBImageOperations {

	public static Function<RGBImage, RGBColor> max() {
		return new Function<RGBImage, RGBColor>() {
			@Override
			public RGBColor apply(RGBImage input) {
				float maxR = 0;
				float maxG = 0;
				float maxB = 0;
				for (RGBColor result : input.colors()) {
					maxR = Math.max(maxR, result.r());
					maxG = Math.max(maxG, result.g());
					maxB = Math.max(maxB, result.b());
				}
				return RGBColor.instance(maxR, maxG, maxB);
			}
		};
	}

	public static Function<RGBImage, RGBColor> min() {
		return new Function<RGBImage, RGBColor>() {
			@Override
			public RGBColor apply(RGBImage input) {
				float minR = 0;
				float minG = 0;
				float minB = 0;
				for (RGBColor result : input.colors()) {
					minR = Math.min(minR, result.r());
					minG = Math.min(minG, result.g());
					minB = Math.min(minB, result.b());
				}
				return RGBColor.instance(minR, minG, minB);
			}
		};
	}
}
