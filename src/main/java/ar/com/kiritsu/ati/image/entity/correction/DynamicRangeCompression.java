package ar.com.kiritsu.ati.image.entity.correction;

import static java.lang.Math.log10;
import static java.lang.Math.max;
import static java.lang.Math.min;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

public class DynamicRangeCompression extends AbstractImageCorrector {

	private float _maxR, _maxG, _maxB;

	public DynamicRangeCompression() {
		super("Compresion dinamica");
	}

	@Override
	public RGBImage apply(RGBImage input) {
		_maxR = 0;
		_maxG = 0;
		_maxB = 0;
		for (RGBColor result : input.colors()) {
			_maxR = Math.max(_maxR, result.r());
			_maxG = Math.max(_maxG, result.g());
			_maxB = Math.max(_maxB, result.b());
		}
		return super.apply(input);
	}

	@Override
	public RGBColor convert(float r, float g, float b) {
		float red = normalize(r, _maxR);
		float green = normalize(g, _maxG);
		float blue = normalize(b, _maxB);
		return RGBColor.instance(red, green, blue);
	}

	private float normalize(float p, float l) {
		l = max(1, l);
		p = max(0, p);
		p = min(1, p);
		float c = (RGBColor.DEPTH - 1) / (float) log10(1 + l); 
		return c * (float) log10(1 + p) / RGBColor.DEPTH;
	}
}
