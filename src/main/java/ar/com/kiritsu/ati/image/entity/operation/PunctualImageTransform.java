package ar.com.kiritsu.ati.image.entity.operation;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.operation.RGBImageColorTransform.ColorAndPostition;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

public class PunctualImageTransform implements ImageTransform {

	private RGBImageColorTransform _transform;

	public PunctualImageTransform() {
	}

	public PunctualImageTransform(RGBImageColorTransform transform) {
		setTransformation(transform);
	}

	public final PunctualImageTransform setTransformation(RGBImageColorTransform transform) {
		_transform = transform;
		return this;
	}

	@Override
	public RGBImage apply(RGBImage input) {
		List<RGBColor> transformed = new ArrayList<>(input.size());
		Vector2I position = new Vector2I();
		int offset = 0;
		for (RGBColor color : input.colors()) {
			position.setX(input.offsetToColumn(offset));
			position.setY(input.offsetToRow(offset));
			offset++;
			transformed.add(_transform.apply(new ColorAndPostition(color, position)));
		}
		return new RGBImage(input.width(), input.height(), transformed);
	}

	@Override
	public String name() {
		return "Transformacion puntual";
	}
}
