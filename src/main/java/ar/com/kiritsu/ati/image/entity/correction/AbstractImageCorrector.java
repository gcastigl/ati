package ar.com.kiritsu.ati.image.entity.correction;

import ar.com.kiritsu.ati.image.entity.operation.PunctualImageTransform;
import ar.com.kiritsu.ati.image.entity.operation.RGBImageColorTransform;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

public abstract class AbstractImageCorrector implements ImageTransform {

	private PunctualImageTransform _operation;
	private String _name;

	public AbstractImageCorrector(String name) {
		_name = name;
		_operation = new PunctualImageTransform(new RGBImageColorTransform() {
			@Override
			public RGBColor apply(ColorAndPostition input) {
				return convert(input.color().r(), input.color().g(), input.color().b());
			}
		});
	}

	@Override
	public RGBImage apply(RGBImage input) {
		return _operation.apply(input);
	}

	public abstract RGBColor convert(float r, float g, float b);

	@Override
	public String name() {
		return _name;
	}

}
