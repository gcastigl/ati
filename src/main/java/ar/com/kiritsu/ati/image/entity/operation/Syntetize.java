package ar.com.kiritsu.ati.image.entity.operation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.Vector2I;
import ar.com.kiritsu.ati.image.entity.operation.api.PairRGBColorTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Preconditions;

public class Syntetize {

	private final List<RGBImage> _images;
	private final Vector2I _dim;
	private final PairRGBColorTransform _syntetizer;

	public Syntetize(List<RGBImage> images, PairRGBColorTransform syntetizer) {
		_images = images;
		_syntetizer = syntetizer;
		_dim = images.get(0).dimentions();
		for (RGBImage image : _images) {
			Preconditions.checkArgument(_dim.equals(image.dimentions()));
		}
	}

	public RGBImage build() {
		int size = _dim.column() * _dim.row();
		List<RGBColor> colors = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			RGBColor result = null;
			for (RGBImage image : _images) {
				RGBColor color = image.colors().get(i);
				if (result == null) {
					result = color;
				} else {
					_syntetizer.apply(Pair.of(result, color));
				}
			}
			colors.add(result);
		}
		return new RGBImage(_dim, colors);
	}

}
