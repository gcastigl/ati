package ar.com.kiritsu.ati.image.entity.operation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import ar.com.kiritsu.ati.image.entity.operation.api.ImagePairTransform;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.operation.api.PairRGBColorTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.util.SerializableSupplier;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

@SuppressWarnings("serial")
public class PunctualImagePairTransform implements ImagePairTransform {

	private final MutablePair<RGBColor, RGBColor> colorPair = new MutablePair<>();
	
	private String _name;
	private PairRGBColorTransform _operation;
	private SerializableSupplier<ImageTransform> _corrector;

	public PunctualImagePairTransform(String name, PairRGBColorTransform operation, SerializableSupplier<ImageTransform> corrector) {
		_name = name;
		_corrector = Preconditions.checkNotNull(corrector);
		_operation = Preconditions.checkNotNull(operation);
	}

	@Override
	public RGBImage apply(Pair<RGBImage, RGBImage> input) {
		System.out.println("Operation started....");
		RGBImage first = input.getRight();
		RGBImage second = input.getLeft();
		Preconditions.checkArgument(first.dimentions().equals(second.dimentions()));
		List<RGBColor> pixels = new ArrayList<>(first.size());
		List<RGBColor> firstPixels = first.colors();
		List<RGBColor> secondPixels = second.colors();
		for (int i = 0; i < firstPixels.size(); i++) {
			RGBColor c1 = firstPixels.get(i);
			RGBColor c2 = secondPixels.get(i);
			pixels.add(combine(c1, c2));
		}
		System.out.println("Starting color correction stage...");
		RGBImage fixed = corrector().get().apply(new RGBImage(first.dimentions(), pixels));
		System.out.println("Operation finished");
		return fixed;
	}

	public RGBColor combine(RGBColor c1, RGBColor c2) {
		colorPair.setLeft(c1);
		colorPair.setRight(c2);
		return _operation.apply(colorPair);
	}

	@Override
	public String getName() {
		return _name;
	}

	public Supplier<ImageTransform> corrector() {
		return _corrector;
	}
}
