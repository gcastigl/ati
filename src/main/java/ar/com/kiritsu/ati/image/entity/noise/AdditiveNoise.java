package ar.com.kiritsu.ati.image.entity.noise;

public class AdditiveNoise extends AbstractImageNoise {

	public AdditiveNoise() {
		super("Ruido Aditivo");
	}

	@Override
	public float randomize(float n) {
		return n + generator().get();
	}

}
