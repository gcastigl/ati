package ar.com.kiritsu.ati.image.entity.operation;

import static ar.com.kiritsu.ati.image.entity.rgb.RGBColors.r;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Collections2.filter;

import java.util.Collection;

import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

public class ImageThresholding implements ImageTransform {

	private static final int MAX_INTERATIONS = 15;
	private final float _t;
	private final float _dt;

	public ImageThresholding(float t, float dt) {
		_t = t;
		_dt = dt;
		Preconditions.checkArgument(0 <= _t && _t <= 1);
		Preconditions.checkArgument(_dt > 0);
	}

	@Override
	public RGBImage apply(RGBImage input) {
		RGBImage result = input.copy();
		boolean differenceIsLessThanDt = false;
		int iterations = 0;
		float t = _t;
		Function<RGBColor, Float> colorBand = r();
		do {
			Predicate<RGBColor> colorBandIsLessThanT = new ColorBandIsLessThan(t, colorBand);
			Collection<RGBColor> g1 = filter(result.colors(), colorBandIsLessThanT);
			Collection<RGBColor> g2 = filter(result.colors(), not(colorBandIsLessThanT));
			float m1 = calcM(g1, colorBand) / g1.size();
			float m2 = calcM(g2, colorBand) / g2.size();
			float newT = (m1 + m2) / 2;
			differenceIsLessThanDt = Math.abs(t - newT) <= _dt || iterations == MAX_INTERATIONS;
			t = newT;
			iterations++;
			if (differenceIsLessThanDt) {
				System.out.println(t);
				g1 = Lists.newLinkedList(g1);
				g2 = Lists.newLinkedList(g2);
				copy(RGBColor.black(), g1);
				copy(RGBColor.white(), g2);
			}
		} while (!differenceIsLessThanDt);
		return result;
	}

	private float calcM(Collection<RGBColor> g, Function<RGBColor, Float> colorBand) {
		float m = 0;
		for (RGBColor color : g) {
			m += colorBand.apply(color);
		}
		return m;
	}

	private void copy(RGBColor color, Collection<RGBColor> colors) {
		for (RGBColor c : colors) {
			c.set(color.r(), color.g(), color.b());
		}
	}

	@Override
	public String name() {
		return "Umbralizacion Global";
	}

	private static class ColorBandIsLessThan implements Predicate<RGBColor> {

		private final Function<RGBColor, Float> _colorBand;
		private final float _t;

		public ColorBandIsLessThan(final float t, Function<RGBColor, Float> colorBand) {
			_colorBand = colorBand;
			_t = t;
		}

		@Override
		public boolean apply(RGBColor input) {
			return _colorBand.apply(input) < _t;
		}

	}
}
