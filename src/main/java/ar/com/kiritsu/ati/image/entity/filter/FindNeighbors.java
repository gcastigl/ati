package ar.com.kiritsu.ati.image.entity.filter;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Optional;

public class FindNeighbors {

	private PixelNeighbors _neighbors;
	private RGBImage _image;

	public FindNeighbors(RGBImage image, int sideLen) {
		_image = image;
		_neighbors = new PixelNeighbors(sideLen);
	}

	public PixelNeighbors at(int row, int column) {
		_neighbors.clear();
		int start, end;
		if (_neighbors.width() == 2) { // special case
			start = 0;
			end = 1;
		} else {
			end = (_neighbors.width() / 2);
			start = -end;
		}
		for (int offsetRow = start; offsetRow <= end; offsetRow++) {
			int rowN = row + offsetRow;
			for (int offsetColumn = start; offsetColumn <= end; offsetColumn++) {
				int columnN = column + offsetColumn;
				_neighbors.set(offsetRow, offsetColumn, getColotAt(rowN, columnN));
			}
		}
		return _neighbors;
	}

	private Optional<RGBColor> getColotAt(int row, int column) {
		if (row < 0 || row >= _image.height() || column < 0 || column >= _image.width()) {
			return RGBColor.absent();
		}
		return Optional.of(_image.colorAt(row, column));
	}

}
