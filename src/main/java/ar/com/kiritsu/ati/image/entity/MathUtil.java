package ar.com.kiritsu.ati.image.entity;

public class MathUtil {

	public static final float capped(float value, float min, float max) {
		return Math.max(min, Math.min(max, value));
	}

	public static final float norm2(float x, float y) {
		return (float) Math.sqrt(x * x + y * y);
	}

	public static final float norm2(float x, float y, float z) {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

}
