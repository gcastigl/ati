package ar.com.kiritsu.ati.image.entity.operation;

import static com.google.common.base.Functions.compose;
import ar.com.kiritsu.ati.image.entity.filter.GaussianFilter;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;
import ar.com.kiritsu.ati.image.entity.tp3.HysterisSupression;
import ar.com.kiritsu.ati.image.entity.tp3.NonMaximumSuppression;

import com.google.common.base.Function;

public class Canny implements ImageTransform {

	private Function<RGBImage, RGBImage> _canny;

	public Canny(int size, final float sigma, final float t1, final float t2) {
		_canny = compose(
			compose(new HysterisSupression(t1, t2), new NonMaximumSuppression()), new GaussianFilter(size, sigma)
		);
		
		_canny = new ImageTransform() {
			@Override
			public String name() {
				return "Wrapper para canny";
			}
			
			@Override
			public RGBImage apply(RGBImage input) {
				// size = 16, anda bien
				return new CannyR(size, sigma, t1, t2).apply(input);
			}
		};
	}

	@Override
	public RGBImage apply(RGBImage input) {
		return _canny.apply(input); 
	}

	@Override
	public String name() {
		return "Canny";
	}

}
