package ar.com.kiritsu.ati.image.entity;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.correction.DynamicRangeCompression;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTracker;
import ar.com.kiritsu.ati.image.entity.operation.api.ImageTransform;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

@SuppressWarnings("serial")
public class ImageTransformHandle implements Serializable {

	private RGBImage _original;
	private RGBImage _modified;
	private Optional<List<RGBImage>> _extraImages;
	// Horrible quick fix to show animated gif
	private File _animationFile;
	
	ImageTransformHandle() {
	}

	public ImageTransformHandle(RGBImage original) {
		setOriginal(original);
	}

	public RGBImage modified() {
		return _modified;
	}

	public ImageTransformHandle setModified(RGBImage modified) {
		_modified = modified;
		return this;
	}

	public RGBImage original() {
		return _original;
	}

	public final void setOriginal(RGBImage original) {
		_original = original;
		restart();
	}

	public void restart() {
		_modified = _original;
		_extraImages = Optional.absent();
	}

	public void handle(Function<RGBImage, RGBImage> transform) {
		try {
			System.out.println("Aplicando: " + name(transform));
			_modified = transform.apply(_modified);
			System.out.println("OK: " + name(transform));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR AL APLICAR TRANSFORMACION -> IGN: " + name(transform));
		}
	}

	public void handle(ImageTracker transform, String pathname) {
		_animationFile = new File(pathname);
		_animationFile = transform.apply(_animationFile);
	}

	public RGBImage handleExtraView(Function<RGBImage, RGBImage> transform) {
		try {
			List<RGBImage> extraImages;
			if (_extraImages.isPresent()) {
				extraImages = _extraImages.get();
			} else {
				_extraImages = Optional.of(extraImages = Lists.newLinkedList());
			}
			System.out.println("Aplicando: " + name(transform));
			extraImages.add(new DynamicRangeCompression().apply(transform.apply(_modified)));
			System.out.println("OK: " + name(transform));
			return Iterables.getLast(extraImages);
		} catch (Exception e) {
			System.out.println("ERROR AL APLICAR TRANSFORMACION -> IGN: " + name(transform));
			throw new IllegalStateException(e);
		}
	}

	public Optional<List<RGBImage>> extraImages() {
		return _extraImages;
	}

	private String name(Function<RGBImage, RGBImage> function) {
		return (function instanceof ImageTransform) ? ((ImageTransform) function).name() : "-";
	}
	
	public File animationFile() {
		return _animationFile;
	}
}
