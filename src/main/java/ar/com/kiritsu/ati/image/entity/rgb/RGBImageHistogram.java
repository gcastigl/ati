package ar.com.kiritsu.ati.image.entity.rgb;


public class RGBImageHistogram {

	private final int[] redHist;
	private final int[] greenHist;
	private final int[] blueHist;
	private final int totalPixels;

	public RGBImageHistogram(RGBImage image) {
		totalPixels = image.size();
		redHist = new int[RGBColor.DEPTH];
		greenHist = new int[RGBColor.DEPTH];
		blueHist = new int[RGBColor.DEPTH];
		for (RGBColor color : image.colors()) {
			increment(redHist, color.redByte());
			increment(greenHist, color.greenByte());
			increment(blueHist, color.blueByte());
		}
	}

	private void increment(int[] histogram, int index) {
		histogram[index]++;
	}

	public int[] getRedHist() {
		return redHist;
	}

	public int[] getBlueHist() {
		return blueHist;
	}

	public int[] getGreenHist() {
		return greenHist;
	}

	public int getTotalPixels() {
		return totalPixels;
	}

}
