package ar.com.kiritsu.ati.image.entity.filter;

import java.util.ArrayList;
import java.util.List;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class PixelNeighbors {

	private final List<ColorAndMaskPosition> _neighbors;
	private final int _width;

	public PixelNeighbors(int width) {
		Preconditions.checkArgument(width > 0);
		_width = width;
		_neighbors = new ArrayList<>(totalSize());
		for (int i = 0; i < totalSize(); i++) {
			_neighbors.add(new ColorAndMaskPosition(0, 0, null));
		}
	}

	public void set(int rowInMask, int columnInMask, Optional<RGBColor> color) {
		int fixedRow, fixedColumn;
		if (width() % 2 == 1) {
			int halfWidth = width() / 2;
			Preconditions.checkArgument(Math.abs(columnInMask) <= halfWidth);
			Preconditions.checkArgument(Math.abs(rowInMask) <= halfWidth);
			fixedRow = rowInMask + halfWidth;
			fixedColumn = columnInMask + halfWidth;
		} else {
			Preconditions.checkArgument(width() == 2);	// Only 2 is supported
			fixedRow = rowInMask;
			fixedColumn = columnInMask;
		}
		ColorAndMaskPosition colorAndPos = _neighbors.get(fixedRow * width() + fixedColumn);
		colorAndPos._rowInMask = rowInMask;
		colorAndPos._columnInMask = columnInMask;
		colorAndPos._color = color;
	}

	public Optional<RGBColor> get(int rowInMask, int columnInMask) {
		int fixedRow, fixedColumn;
		if (width() % 2 == 1) {
			int halfWidth = width() / 2;
			Preconditions.checkArgument(Math.abs(columnInMask) <= halfWidth);
			Preconditions.checkArgument(Math.abs(rowInMask) <= halfWidth);
			fixedRow = rowInMask + halfWidth;
			fixedColumn = columnInMask + halfWidth;
		} else {
			Preconditions.checkArgument(width() == 2);	// Only 2 is supported
			fixedRow = rowInMask;
			fixedColumn = columnInMask;
		}
		return _neighbors.get(fixedRow * width() + fixedColumn).color();
	}

	public void clear() {
		for (int i = 0; i < totalSize(); i++) {
			ColorAndMaskPosition colorAndPos = _neighbors.get(i);
			colorAndPos._color = null;
			colorAndPos._rowInMask = 0;
			colorAndPos._columnInMask = 0;
		}
	}

	public int width() {
		return _width;
	}

	public int totalSize() {
		return width() * width();
	}

	public int totalSizeIgnoringMiddle() {
		return totalSize() - 1;
	}

	public List<ColorAndMaskPosition> valuesWithMiddle() {
		return _neighbors;
	}

	public List<ColorAndMaskPosition> valuesWithoutMiddle() {
		List<ColorAndMaskPosition> withoutMiddle = new ArrayList<ColorAndMaskPosition>(_neighbors);
		int halfWidth = width() / 2;
		int fixedRow = 0 + halfWidth;
		int fixedColumn = 0 + halfWidth;
		ColorAndMaskPosition removed = withoutMiddle.remove(fixedRow * width() + fixedColumn);
		Preconditions.checkArgument(removed.rowInMask() == 0 && removed.columnInMask() == 0);
		return withoutMiddle;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (List<ColorAndMaskPosition> row : Lists.partition(_neighbors, width())) {
			builder.append(row.toString() + "\n");
		}
		return builder.toString();
	}
	
	
	public static class ColorAndMaskPosition {
		private int _rowInMask, _columnInMask;
		private Optional<RGBColor> _color;

		public ColorAndMaskPosition(int row, int column, Optional<RGBColor> color) {
			_rowInMask = row;
			_columnInMask = column;
			_color = color;
		}

		public Optional<RGBColor> color() {
			return _color;
		}

		public int rowInMask() {
			return _rowInMask;
		}

		public int columnInMask() {
			return _columnInMask;
		}

		@Override
		public String toString() {
			return color().isPresent() ? color().get().toString() : color().toString();
		}
	}
}
