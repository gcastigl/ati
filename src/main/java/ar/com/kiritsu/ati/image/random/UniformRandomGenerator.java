package ar.com.kiritsu.ati.image.random;

import com.google.common.base.Preconditions;

public class UniformRandomGenerator implements RandomGenerator {

	private float _min, _max;

	public UniformRandomGenerator(float min, float max) {
		Preconditions.checkArgument(min <= max);
		_min = min;
		_max = max;
	}

	@Override
	public float get() {
		float x = (float) Math.random();
		return x * (_max - _min) + _min;
	}

}
