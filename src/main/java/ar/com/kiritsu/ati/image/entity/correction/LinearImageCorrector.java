package ar.com.kiritsu.ati.image.entity.correction;

import ar.com.kiritsu.ati.image.entity.rgb.RGBColor;
import ar.com.kiritsu.ati.image.entity.rgb.RGBImage;

public class LinearImageCorrector extends AbstractImageCorrector {

	private float _maxR, _maxG, _maxB;
	private float _minR, _minG, _minB;

	public LinearImageCorrector() {
		super("Compresion linear");
	}

	@Override
	public RGBImage apply(RGBImage input) {
		_maxR = -Float.MAX_VALUE;
		_minR = Float.MAX_VALUE;
		_maxG = -Float.MAX_VALUE;
		_minG = Float.MAX_VALUE;
		_maxB = -Float.MAX_VALUE;
		_minB = Float.MAX_VALUE;
		for (RGBColor result : input.colors()) {
			_maxR = Math.max(_maxR, result.r());
			_minR = Math.min(_minR, result.r());
			_maxG = Math.max(_maxG, result.g());
			_minG = Math.min(_minG, result.g());
			_maxB = Math.max(_maxB, result.b());
			_minB = Math.min(_minB, result.b());
		}
		return super.apply(input);
	}

	@Override
	public RGBColor convert(float r, float g, float b) {
		float newR = normalize(r, _minR, _maxR);
		float newG = normalize(g, _minG, _maxG);
		float newB = normalize(g, _minB, _maxB);
		return RGBColor.instance(newR, newG, newB);
	}

	private float normalize(float r, float min, float max) {
		if (max == min) {
			return r;
		}
		return (r - min) / (max - min);
	}
}
